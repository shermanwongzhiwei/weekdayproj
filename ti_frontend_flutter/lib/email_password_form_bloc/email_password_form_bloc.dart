import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:ti_frontend_flutter/controllers/validators.dart';
import 'package:ti_frontend_flutter/email_password_form_bloc/email_password_form_event.dart';
import 'package:ti_frontend_flutter/email_password_form_bloc/email_password_form_state.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

class EmailPasswordFormBloc extends Bloc<EmailPasswordFormEvent, EmailPasswordFormState> {
  final UserRepository userRepository;

  EmailPasswordFormBloc({@required this.userRepository});

  @override
  EmailPasswordFormState get initialState => EmailPasswordFormState.empty();

  @override
  Stream<EmailPasswordFormState> transformEvents(
    Stream<EmailPasswordFormEvent> events,
    Stream<EmailPasswordFormState> Function(EmailPasswordFormEvent event) next,
  ) {
    final nonDebounceEmailStream = events.where((events) {
      return events is! FormEmailChanged && events is! FormPasswordChanged;
    });
    final debounceEmailStream = events.where((event) {
      return event is FormEmailChanged;
    }).debounce((_) => TimerStream(true, Duration(milliseconds: 300)));
    final debouncePasswordStream = events.where((event) {
      return event is FormPasswordChanged;
    }).debounce((_) => TimerStream(true, Duration(milliseconds: 300)));

    return super.transformEvents(
        nonDebounceEmailStream.mergeWith([debounceEmailStream, debouncePasswordStream]), next);
  }

  @override
  Stream<EmailPasswordFormState> mapEventToState(EmailPasswordFormEvent event) async* {
    logger.d('Email Password Event: ${event.runtimeType}');
    if (event is SignUpFormSubmitted) {
      yield* _mapSignUpFormSubmittedToState(event.email, event.password);
    } else if (event is SignInFormSubmitted) {
      yield* _mapSignInFormSubmittedToState(event.email, event.password);
    } else if (event is FormEmailChanged) {
      yield* _mapSignUpEmailChangedToState(event.email);
    } else if (event is FormPasswordChanged) {
      yield* _mapSignUpPasswordChangedToState(event.password);
    }
  }

  Stream<EmailPasswordFormState> _mapSignUpFormSubmittedToState(String email, String password) async* {
    yield EmailPasswordFormState.loading();
    try {
      await userRepository.signUp(email: email.trim(), password: password);
      yield EmailPasswordFormState.successFirstTime();
    } catch (e) {
      logger.e('Sign up form submmited: $e');
      yield EmailPasswordFormState.failure();
    }
  }

  Stream<EmailPasswordFormState> _mapSignInFormSubmittedToState(String email, String password) async* {
    yield EmailPasswordFormState.loading();
    try {
      await userRepository.signInWithEmailAndPassword(email: email.trim(), password: password);
      if (await userRepository.isFirstTime()) {
        yield EmailPasswordFormState.successFirstTime();
      } else {
        yield EmailPasswordFormState.successNonFirstTime();
      }
    } catch (e) {
      logger.e('Sign in form submmited: $e');
      yield EmailPasswordFormState.failure();
    }
  }

  Stream<EmailPasswordFormState> _mapSignUpEmailChangedToState(String email) async* {
    yield state.update(
      emailErrorMessage: Validators.emailErrorMessage(email),
    );
  }

  Stream<EmailPasswordFormState> _mapSignUpPasswordChangedToState(String password) async* {
    yield state.update(
      passwordErrorMessage: Validators.passwordErrorMessage(password),
    );
  }
}
