import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
// not sure need anot Equatable
abstract class EmailPasswordFormEvent extends Equatable {
  final List<Object> props;

  EmailPasswordFormEvent([this.props = const []]);
}

class FormEmailChanged extends EmailPasswordFormEvent {
  final String email;

  FormEmailChanged({@required this.email}) : super([email]);
}

class FormPasswordChanged extends EmailPasswordFormEvent {
  final String password;

  FormPasswordChanged({@required this.password}) : super([password]);
}

class SignUpFormSubmitted extends EmailPasswordFormEvent {
  final String email;
  final String password;

  SignUpFormSubmitted({this.email, this.password}) : super([email, password]);
}

class SignInFormSubmitted extends EmailPasswordFormEvent {
  final String email;
  final String password;

  SignInFormSubmitted({this.email, this.password}) : super([email, password]);
}