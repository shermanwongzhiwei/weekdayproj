import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

@immutable
class EmailPasswordFormState extends Equatable {
  final String emailErrorMessage;
  final String passwordErrorMessage;
  final bool isSubmitting;
  final bool isFirstTime;
  final bool isSuccess;
  final bool isFailure;

  @override
  String toString() {
    return '''EmailPasswordFormState {
    emailErrorMessage: $emailErrorMessage ,
    passwordErrorMessage: $passwordErrorMessage,
    isSubmitting: $isSubmitting,
    isFirstTime: $isFirstTime,
    isSuccess: $isSuccess,
    isFailure $isFailure
    }''';
  }

  @override
  List<Object> get props =>
      [emailErrorMessage, passwordErrorMessage, isSubmitting, isFirstTime, isSuccess, isFailure];

  EmailPasswordFormState({
    @required this.emailErrorMessage,
    @required this.passwordErrorMessage,
    @required this.isSubmitting,
    @required this.isFirstTime,
    @required this.isSuccess,
    @required this.isFailure,
  }) {
    logger.d(this);
  }

  factory EmailPasswordFormState.empty() {
    return EmailPasswordFormState(
      emailErrorMessage: null,
      passwordErrorMessage: null,
      isSubmitting: false,
      isFirstTime: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory EmailPasswordFormState.loading() {
    return EmailPasswordFormState(
      emailErrorMessage: null,
      passwordErrorMessage: null,
      isSubmitting: true,
      isFirstTime: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory EmailPasswordFormState.failure() {
    return EmailPasswordFormState(
      emailErrorMessage: null,
      passwordErrorMessage: null,
      isSubmitting: false,
      isFirstTime: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory EmailPasswordFormState.successFirstTime() {
    return EmailPasswordFormState(
      emailErrorMessage: null,
      passwordErrorMessage: null,
      isSubmitting: false,
      isFirstTime: true,
      isSuccess: true,
      isFailure: false,
    );
  }

  factory EmailPasswordFormState.successNonFirstTime() {
    return EmailPasswordFormState(
      emailErrorMessage: null,
      passwordErrorMessage: null,
      isSubmitting: false,
      isFirstTime: false,
      isSuccess: true,
      isFailure: false,
    );
  }

  EmailPasswordFormState update({String emailErrorMessage, String passwordErrorMessage}) {
    logger.d('FormValidationState updated');
    logger.i(passwordErrorMessage);

    return copyWith(
      emailErrorMessage: emailErrorMessage,
      passwordErrorMessage: passwordErrorMessage,
      isSubmitting: false,
      isFirstTime: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  EmailPasswordFormState copyWith({
    String emailErrorMessage,
    String passwordErrorMessage,
    bool isSubmitting,
    bool isFirstTime,
    bool isSuccess,
    bool isFailure,
  }) {
    logger.i(passwordErrorMessage);
    return EmailPasswordFormState(
      emailErrorMessage: emailErrorMessage,
      passwordErrorMessage: passwordErrorMessage,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isFirstTime: isFirstTime ?? this.isFirstTime,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
    );
  }
}
