import 'package:google_maps_webservice/places.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class AddLocationEvent extends Equatable {
  final List<Object> props;

  AddLocationEvent([this.props = const []]);
}

class QueryChanged extends AddLocationEvent {
  final String query;

  QueryChanged({
    @required this.query,
  }): super([query]);
}

class SuggestionTapped extends AddLocationEvent {
  final PlacesSearchResult suggestion;

  SuggestionTapped({
    @required this.suggestion,
  }): super([suggestion]);
}