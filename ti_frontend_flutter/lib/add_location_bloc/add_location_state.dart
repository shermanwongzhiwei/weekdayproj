import 'package:flutter/cupertino.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
class AddLocationState extends Equatable {
  final bool isSearching;
  final List<PlacesSearchResult> suggestions;
  final bool onTap;
  final PlacesSearchResult place;

  @override
  String toString() {
    return '''AddLocationState {
    isSearching: $isSearching,
    suggestions: $suggestions,
    onTap: $onTap,
    place: $place
    }''';
  }

  @override
  List<Object> get props => [
    isSearching, 
    suggestions, 
    onTap, 
    place,
  ];

  AddLocationState({
    @required this.isSearching,
    @required this.suggestions,
    @required this.onTap,
    @required this.place,
  });

  factory AddLocationState.empty() {
    return AddLocationState(
      isSearching: false,
      suggestions: List<PlacesSearchResult>(),
      onTap: false,
      place: null,
    );
  }

  factory AddLocationState.searching(List<PlacesSearchResult> suggestions) {
    return AddLocationState(
      isSearching: true,
      suggestions: suggestions,
      onTap: false,
      place: null,
    );
  }

  factory AddLocationState.tapped(PlacesSearchResult place) {
    return AddLocationState(
      isSearching: false,
      suggestions: List<PlacesSearchResult>(),
      onTap: true,
      place: place,
    );
  }

  AddLocationState update({bool isSearching, bool onTap, List<PlacesSearchResult> suggestions, PlacesSearchResult place}) {
    return copyWith(
      isSearching: isSearching,
      onTap: onTap,
      suggestions: suggestions,
      place: place,
    );
  }

  AddLocationState copyWith({
    bool isSearching,
    bool onTap,
    List<PlacesSearchResult> suggestions,
    PlacesSearchResult place,
  }) {
    return AddLocationState(
      isSearching: isSearching ?? this.isSearching,
      onTap: onTap ?? this.onTap,
      suggestions: suggestions ?? this.suggestions,
      place: place ?? this.place,
    );
  }
}