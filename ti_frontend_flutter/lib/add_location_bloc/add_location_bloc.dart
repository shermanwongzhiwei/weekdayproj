import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import "package:google_maps_webservice/places.dart";
import 'package:ti_frontend_flutter/add_location_bloc/bloc.dart';

class AddLocationBloc extends Bloc<AddLocationEvent, AddLocationState> {
  final GoogleMapsPlaces _places;
  
  AddLocationBloc({GoogleMapsPlaces places})
    :  _places = places ?? GoogleMapsPlaces(apiKey: 'AIzaSyAATfNUnK6aLJK_I67NwwoiCdBwrdFFDmQ');

  @override
  AddLocationState get initialState => AddLocationState.empty();

  @override
  Stream<AddLocationState> transformEvents(
    Stream<AddLocationEvent> events,
    Stream<AddLocationState> Function(AddLocationEvent event) next,
  ) {
    final nonDebounceStream = events.where((event) {
      return event is SuggestionTapped;
    });

    final debounceQueryChangedStream = events.where((event) {
      return event is QueryChanged;
    }).debounce((_) => TimerStream(true, Duration(milliseconds: 1000)));

    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceQueryChangedStream]), 
      next
    );
  }

  @override
  Stream<AddLocationState> mapEventToState(AddLocationEvent event) async* {
    if (event is QueryChanged) {
      yield* _mapQueryChangedToState(event.query);
    } else if (event is SuggestionTapped) {
      yield* _mapSuggestionTappedToState(event.suggestion);
    }
  }

  Stream<AddLocationState> _mapQueryChangedToState(String query) async* {
    if (query.isEmpty) {
      yield state.update(isSearching: false, suggestions: List<PlacesSearchResult>(), onTap: false);
    } else if (state.place == null || query != state.place.name){
      List<PlacesSearchResult> searchList = await _getSuggestions(query) ?? List<String>();
      yield state.update(isSearching: true, suggestions: searchList, onTap: false);
    } else {
      yield state.update(isSearching: false, suggestions: List<PlacesSearchResult>());
    }
  }

  Future<List<PlacesSearchResult>> _getSuggestions(String query) async {
    PlacesSearchResponse response = await _places.searchByText(query);
    return response.results;
  }

  Stream<AddLocationState> _mapSuggestionTappedToState(PlacesSearchResult restaurant) async* {
    yield AddLocationState.tapped(restaurant);
  }
}