import 'package:google_maps_webservice/places.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

@immutable
abstract class PostRecommendationEvent extends Equatable {
  final List<Object> props;

  PostRecommendationEvent([this.props = const []]);
}

class Post extends PostRecommendationEvent {
  final String description;
  final String dish;
  final PlacesSearchResult restaurant;
  final List<Asset> images;

  Post({
    @required this.description,
    @required this.dish,
    @required this.restaurant,
    @required this.images,
  }): super([description, dish, restaurant, images]);
}

class AddImages extends PostRecommendationEvent {
  final List<Asset> currentImages;

  AddImages({
    @required this.currentImages,
  });
}