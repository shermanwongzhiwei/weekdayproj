import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';

@immutable
class PostRecommendationState extends Equatable {
  final bool isPosting;
  final bool isSuccess;
  final bool isFailure;
  final bool isRetrieveImagesSuccess;
  final bool isRetrieveImagesFailure;
  final List<Asset> images;
  final String retrieveImagesError;
  final Recommendation recommendation;

  @override
  String toString() {
    return '''PostRecommendationState {
    isPosting: $isPosting,
    isSuccess: $isSuccess,
    isFailure: $isFailure,
    isRetrieveImagesSuccess: $isRetrieveImagesSuccess,
    isRetrieveImagesFailure: $isRetrieveImagesFailure,
    images: $images,
    retrieveImagesError: $retrieveImagesError,
    recommendation: $recommendation
    }''';
  }

  @override
  List<Object> get props => [
    isPosting, 
    isSuccess, 
    isFailure, 
    isRetrieveImagesSuccess,
    isRetrieveImagesFailure,
    images,
    retrieveImagesError,
    recommendation,
  ];

  PostRecommendationState({
    @required this.isPosting,
    @required this.isSuccess,
    @required this.isFailure,
    @required this.isRetrieveImagesSuccess,
    @required this.isRetrieveImagesFailure,
    @required this.images,
    @required this.retrieveImagesError,
    @required this.recommendation,
  });

  factory PostRecommendationState.empty() {
    return PostRecommendationState(
      isPosting: false,
      isSuccess: false,
      isFailure: false,
      isRetrieveImagesSuccess: false,
      isRetrieveImagesFailure: false,
      images: List<Asset>(),
      retrieveImagesError: null,
      recommendation: null,
    );
  }

  factory PostRecommendationState.loading() {
    return PostRecommendationState(
      isPosting: true,
      isSuccess: false,
      isFailure: false,
      isRetrieveImagesSuccess: false,
      isRetrieveImagesFailure: false,
      images: List<Asset>(),
      retrieveImagesError: null,
      recommendation: null,
    );
  }

  factory PostRecommendationState.failure() {
    return PostRecommendationState(
      isPosting: false,
      isSuccess: false,
      isFailure: true,
      isRetrieveImagesSuccess: false,
      isRetrieveImagesFailure: false,
      images: List<Asset>(),
      retrieveImagesError: null,
      recommendation: null,
    );
  }

  factory PostRecommendationState.success(Recommendation recommendation) {
    return PostRecommendationState(
      isPosting: false,
      isSuccess: true,
      isFailure: false,
      isRetrieveImagesSuccess: false,
      isRetrieveImagesFailure: false,
      images: List<Asset>(),
      retrieveImagesError: null,
      recommendation: recommendation,
    );
  }

  factory PostRecommendationState.retrieveImagesSuccess(List<Asset> images) {
    return PostRecommendationState(
      isPosting: false,
      isSuccess: false,
      isFailure: false,
      isRetrieveImagesSuccess: true,
      isRetrieveImagesFailure: false,
      images: images,
      retrieveImagesError: null,
      recommendation: null,
    );
  }

  factory PostRecommendationState.retrieveImagesFailure(String error) {
    return PostRecommendationState(
      isPosting: false,
      isSuccess: false,
      isFailure: false,
      isRetrieveImagesSuccess: false,
      isRetrieveImagesFailure: true,
      images: List<Asset>(),
      retrieveImagesError: error,
      recommendation: null,
    );
  }
}