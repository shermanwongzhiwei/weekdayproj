import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import "package:google_maps_webservice/places.dart";
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:ti_frontend_flutter/post_recommendation_bloc/bloc.dart';
import 'package:ti_frontend_flutter/services/recommendation_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';

class PostRecommendationBloc extends Bloc<PostRecommendationEvent, PostRecommendationState> {
  final RecommendationRepository _recommendationRepository;
  
  PostRecommendationBloc({RecommendationRepository recommendationRepository, GoogleMapsPlaces places})
    : _recommendationRepository = recommendationRepository;

  @override
  PostRecommendationState get initialState => PostRecommendationState.empty();

  @override
  Stream<PostRecommendationState> transformEvents(
    Stream<PostRecommendationEvent> events,
    Stream<PostRecommendationState> Function(PostRecommendationEvent event) next,
  ) {
    final nonDebounceStream = events.where((event) {
      return event is AddImages;
    });

    final debouncePostStream = events.where((event) {
      return event is Post;
    }).debounce((_) => TimerStream(true, Duration(milliseconds: 3000)));

    return super.transformEvents(
      nonDebounceStream.mergeWith([debouncePostStream]), 
      next
    );
  }

  @override
  Stream<PostRecommendationState> mapEventToState(PostRecommendationEvent event) async* {
    if (event is Post) {
      yield* _mapPostToState(
        description: event.description,
        dish: event.dish,
        restaurant: event.restaurant,
        images: event.images,
      );
    } else if (event is AddImages) {
      print('Add images identified');
      yield* _mapAddImagesToState(event.currentImages);
    }
  }

  Stream<PostRecommendationState> _mapPostToState({String description, String dish, PlacesSearchResult restaurant, List<Asset> images}) async* {
    yield PostRecommendationState.loading();
    try {
      Recommendation recommendation = await _recommendationRepository.postRecommendation(
        description: description,
        dish: dish,
        restaurant: restaurant,
        images: images,
      );
      yield PostRecommendationState.success(recommendation);
    } catch (e) {
      logger.e('PostRecommendationError: $e');
      yield PostRecommendationState.failure();
    }
  }

  Stream<PostRecommendationState> _mapAddImagesToState(List<Asset> images) async* {
    try {
      List<Asset> resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#FFFFFF",
          actionBarTitleColor: "#000000",
          actionBarTitle: "Select Images",
          statusBarColor: "#B80009",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#777777",
          okButtonDrawable: 'baseline_check_black_24dp',
          backButtonDrawable: 'baseline_arrow_back_black_24dp',
        ),
      );
      print(resultList[0].name);
      yield PostRecommendationState.retrieveImagesSuccess(resultList);
    } on Exception catch (e) {
      String error = e.toString();
      yield PostRecommendationState.retrieveImagesFailure(error);
    }
  }
}