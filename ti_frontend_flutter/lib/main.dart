import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ti_frontend_flutter/generated/i18n.dart';
import 'package:ti_frontend_flutter/navigator_observer.dart';
import 'package:ti_frontend_flutter/routes.dart';
import 'package:ti_frontend_flutter/screens/main_bottom_navigation.dart';
import 'package:ti_frontend_flutter/screens/provide_username.dart';
import 'package:ti_frontend_flutter/screens/splash_screen.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/authentication_bloc/bloc.dart';
import 'package:ti_frontend_flutter/screens/sign_in.dart';
import 'package:ti_frontend_flutter/simple_bloc_delegate.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Color(0xFFF40D30)));
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(ZeekApp());
}

class ZeekApp extends StatefulWidget {
  State<ZeekApp> createState() => _ZeekAppState();
}

class _ZeekAppState extends State<ZeekApp> {
  final UserRepository _userRepository = UserRepository();
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = AuthenticationBloc(userRepository: _userRepository);
    _authenticationBloc.add(AppStarted());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _authenticationBloc,
      child: MaterialApp(
        localizationsDelegates: [S.delegate],
        supportedLocales: S.delegate.supportedLocales,
        navigatorObservers: [CustomNavigatorObserver()],
        title: 'zeek',
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          fontFamily: 'OpenSans',
          backgroundColor: Colors.white,
          primaryColor: Color(0xFFF40D30),
          primaryColorLight: Color(0xFFFF5B5A),
          primaryColorDark: Color(0xFFB80009),
          accentColor: Color(0xFFF40D30),
          dialogBackgroundColor: Color(0xFFD3D3D3),
          textTheme: Theme.of(context).textTheme.apply(
            bodyColor: Color(0xFF757575)
          )
        ),
        onGenerateRoute: RouteGenerator.generateRoute,
        home: BlocBuilder(
          bloc: _authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            print(state);
            if (state is Uninitialized) {
              return SplashScreen();
            } else if (state is Unauthenticated) {
              return SignIn(userRepository: _userRepository);
            } else if (state is FirstTimeAuthenticated) {
              return ProvideUsernamePage();
            } else if (state is NonFirstTimeAuthenticated) {
              return MainBottomNavigation(currentUser: state.user);
            }
            return null; // Not reachable
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _authenticationBloc.close();
    super.dispose();
  }
}