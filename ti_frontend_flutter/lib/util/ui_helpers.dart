import 'package:flutter/cupertino.dart';

class UIHelpers {
  static const double margin_big = 30;
  static const double margin_medium = 20;
  static const double margin_small = 10;

  static const double padding_small = 10;
  static const double padding_medium = 20;
  static const double padding_large = 30;

  static const double radius_small = 10;
  static const double radius_medium = 15;
  static const double radius_large = 20;
  static const double radius_xlarge = 30;

  static const SizedBox sBox = SizedBox(height: 10);
  static const SizedBox mBox = SizedBox(height: 20);
  static const SizedBox lBox = SizedBox(height: 30);
}
