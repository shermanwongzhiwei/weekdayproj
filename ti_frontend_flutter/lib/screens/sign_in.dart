import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:ti_frontend_flutter/routes.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/social_media_sign_in_bloc/bloc.dart';
import 'package:ti_frontend_flutter/authentication_bloc/bloc.dart';
import 'package:ti_frontend_flutter/util/ui_helpers.dart';
import 'package:ti_frontend_flutter/email_password_form_bloc/bloc.dart';
import 'package:ti_frontend_flutter/util/logging.dart';
import 'package:ti_frontend_flutter/controllers/validators.dart';
import 'package:form_field_validator/form_field_validator.dart';



class SignIn extends StatefulWidget {
  final UserRepository _userRepository;

  SignIn({Key key, UserRepository userRepository})
      : _userRepository = userRepository,
        super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<SignIn> {
  // from sign_in_email
  EmailPasswordFormBloc formBloc;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final EmailValidator _emailValidator = Validators.getEmailValidator();
  final MultiValidator _passwordValidator = Validators.getPasswordValidator();
  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();
  bool isHidePassword = true;
  bool get isPopulated => emailController.text.isNotEmpty && passwordController.text.isNotEmpty;


  SocialMediaSignInBloc _socialMediaSignInBloc;

  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    super.initState();
    _socialMediaSignInBloc = SocialMediaSignInBloc(userRepository: _userRepository);
    formBloc = EmailPasswordFormBloc(userRepository: widget._userRepository);
    emailController.addListener(_onEmailChanged);
    passwordController.addListener(_onPasswordChanged);
  }


  bool isSignInButtonEnabled(EmailPasswordFormState state) {
    logger.d(state);
    return _emailValidator.isValid(emailController.text) &&
        _passwordValidator.isValid(passwordController.text) &&
        isPopulated &&
        !state.isSubmitting;
  }

  void _signInListener(BuildContext context, EmailPasswordFormState state) {
    if (state.isFailure) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text('Login Failure'), Icon(Icons.error)],
          ),
          backgroundColor: Colors.red,
        ),
      );
    }
    if (state.isSubmitting) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Logging In...'),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    }
    if (state.isSuccess) {
      BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
    }
  }

  void _socialMediaSignInListener(BuildContext context, SocialMediaSignInState state) {
    if (state.isFailure) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text('Login Failure'), Icon(Icons.error)],
          ),
          backgroundColor: Colors.red,
        ),
      );
    }
    if (state.isSubmitting) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Logging In...'),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    }
    if (state.isSuccess) {
      BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocProvider<SocialMediaSignInBloc>(
          create: (BuildContext context) => _socialMediaSignInBloc,
          child: BlocListener(
            bloc: _socialMediaSignInBloc,
            listener: _socialMediaSignInListener,
            child: Container(
              padding: EdgeInsets.all(40),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ListView(
                  // mainAxisSize: MainAxisSize.min,
                  // mainAxisAlignment: MainAxisAlignment.end,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  shrinkWrap: true,
                  children: <Widget>[
                    Center(
                      child: Text(
                        'zeek',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 60,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                    // Spacer(flex: 1),
//                    Column(
//                      mainAxisSize: MainAxisSize.min,
//                      children: <Widget>[
//                        Text(
//                          "We decide what to eat" + ',',
//                          style: TextStyle(
//                            fontFamily: 'OpenSans',
//                            fontWeight: FontWeight.w400,
//                            color: Colors.grey,
//                          ),
//                        ),
//                        Text(
//                          "so you don't have to",
//                          style: TextStyle(
//                            fontFamily: 'OpenSans',
//                            fontWeight: FontWeight.w400,
//                            color: Colors.grey,
//                          ),
//                        ),
//                      ],
//                    ),
//                    Spacer(flex: 1),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical:10.0),
                      child: SignInButton(
                        Buttons.Google,
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        onPressed: () => _socialMediaSignInBloc.add(SignInWithGooglePressed()),
                      ),
                    ),
                    // UIHelpers.mBox,
                    Padding(padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: SignInButton(
                        Buttons.Facebook,
                        text: 'Sign In With Facebook',
                        padding: EdgeInsets.symmetric(vertical: 15.0),
                        onPressed: () => _socialMediaSignInBloc.add(SignInWithFacebookPressed()),
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Text('- OR -')
                      ),
                    ),

                    BlocProvider<EmailPasswordFormBloc>(
                      create: (BuildContext context) => formBloc,
                      child: BlocListener(
                        bloc: formBloc,
                        listener: _signInListener,
                        child: BlocBuilder<EmailPasswordFormBloc, EmailPasswordFormState>(
                          builder: (BuildContext context, EmailPasswordFormState state) {
                            return Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10.0),
                                  child: SizedBox(
                                    height: 70.0,
                                    child: TextFormField(
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.emailAddress,
                                      autocorrect: false,
                                      autovalidate: true,
                                      validator: _emailValidator,
                                      controller: emailController,
                                      onFieldSubmitted: (_) {
                                        emailFocusNode.unfocus();
                                        FocusScope.of(context).requestFocus(passwordFocusNode);
                                      },
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        // helperText: ' ',
                                        labelText: 'Email address'
                                      ),
                                    )
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 10.0),
                                  child: SizedBox(
                                    height: 70.0,
                                    child: TextFormField(
                                      textInputAction: TextInputAction.done,
                                      keyboardType: TextInputType.visiblePassword,
                                      focusNode: passwordFocusNode,
                                      controller: passwordController,
                                      autocorrect: false,
                                      autovalidate: true,
                                      validator: _passwordValidator,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Password'
                                      )
                                    )
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 10.0, top: 20.0),
                                  child: SizedBox(
                                    height: 50.0,
                                    width: 320.0,
                                    child: RaisedButton(
                                      elevation: 0.0,
                                      child: Text('Sign In',
                                        style: TextStyle(
                                          color: Colors.white
                                        )),
                                      // icon: Icons.email,
                                      // iconColor: Theme.of(context).primaryColor,
                                      color: Theme.of(context).primaryColor,
                                      // disabledColor: Theme.of(context).primaryColor,
                                      splashColor: Colors.redAccent,
                                      // onPressed: () {
                                      //   Navigator.pushNamed(context, sign_in_email);
                                      // },
                                      onPressed: isSignInButtonEnabled(state) ? _onFormSubmitted : null,
                                    ),
                                  ),
                                ),

                              ]
                            );
                          }
                        )
                      )
                    ),

                    

                    




                    // UIHelpers.mBox,
                    // SignInButtonBuilder(
                    //   icon: Icons.email,
                    //   iconColor: Colors.white,
                    //   backgroundColor: Theme.of(context).primaryColor,
                    //   textColor: Colors.white,
                    //   text: 'Sign in with Email',
                    //   onPressed: () {
                    //     Navigator.pushNamed(context, sign_in_email);
                    //   },
                    // ),
                    UIHelpers.mBox,
                    // Just to make UI consistent, redirect to sign up page
                    Center(
                      child: InkWell(
                        child: Text('New User? Sign Up Here'),
                        onTap: (){
                          Navigator.pushNamed(context, sign_up_email);
                        }
                      ),
                    ),

                    // SignInButtonBuilder(
                    //   icon: Icons.person_add,
                    //   backgroundColor: Colors.white,
                    //   iconColor: Theme.of(context).primaryColor,
                    //   textColor: Colors.black,
                    //   text: 'Sign up with Email',
                    //   onPressed: () {
                    //     Navigator.pushNamed(context, sign_up_email);
                    //   },
                    // ),
                  ],
                )
              ),
              )
            )
          ),
        ),
      );
  }

  @override
  void dispose() {
    formBloc.close();
    _socialMediaSignInBloc.close();
    super.dispose();
  }

  void _onEmailChanged() {
    formBloc.add(
      FormEmailChanged(email: emailController.text),
    );
  }

  void _onPasswordChanged() {
    logger.i(passwordController.text);
    formBloc.add(
      FormPasswordChanged(password: passwordController.text),
    );
  }

  void _onFormSubmitted() {
    FocusScope.of(context).unfocus();
    formBloc.add(
      SignInFormSubmitted(
        email: emailController.text,
        password: passwordController.text,
      ),
    );
  }
}
