import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ti_frontend_flutter/favourite_bloc/bloc.dart';
import 'package:ti_frontend_flutter/services/favourite_repository.dart';
import 'package:ti_frontend_flutter/widgets/favourite_card.dart';
import 'package:ti_frontend_flutter/routes.dart';

class FavouritePage extends StatefulWidget {
  final FavouriteRepository _favouriteRepository;

  FavouritePage({Key key, FavouriteRepository favouriteRepository})
    : _favouriteRepository = favouriteRepository ?? FavouriteRepository(),
      super(key: key);

  @override
  State<StatefulWidget> createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {
  Completer<void> _refreshCompleter;
  // final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  FavouriteBloc _favouriteBloc;

  FavouriteRepository get _favouriteRepository => widget._favouriteRepository;

  @override
  void initState() {
    super.initState();
    _favouriteBloc = FavouriteBloc(favouriteRepository: _favouriteRepository);
    _favouriteBloc.add(Fetch());
    _scrollController.addListener(_onScroll);
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      listener: (context, state){
        if (state is FavouriteLoaded){
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();
        }
      },
      bloc: _favouriteBloc,
      builder: (BuildContext context, FavouriteState state) {
        if (state is FavouriteUninitialized) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is FavouriteError) {
          return Center(
            child: Text('Failed to fetch favourites'),
          );
        }
        if (state is FavouriteLoaded) {
          if (state.favourites.isEmpty) {
            return Center(
              child: Text('No favourites saved yet'),
            );
          }
          return Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              centerTitle: false,
              backgroundColor: Colors.white,
              title: Text("Favourites", 
                style: TextStyle(
                  fontFamily: 'Montserrat', 
                  fontSize: 30.0,
                  color: Theme.of(context).primaryColor, 
                  fontWeight: FontWeight.bold)),
            ),
            body: RefreshIndicator(
              // key: _refreshIndicatorKey,
              onRefresh: (){
                _favouriteBloc.add(Refresh());
                return _refreshCompleter.future;
              },
              child: ListView.builder(
                itemCount: state.favourites.length * 2,
                itemBuilder: (BuildContext context, int itemIndex) {
                  final int index = itemIndex ~/ 2;

                  if (itemIndex.isOdd){
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
                      child: Divider(height: 0, color: Colors.grey)
                    );
                  }

                  return index >= state.favourites.length
                      ? null
                      : InkWell(
                        child: FavouriteCard(favourite: state.favourites[index]),
                        onTap: () {
                          Navigator.pushNamed(context, recommendation_details, arguments: state.favourites[index].recommendation);
                        },
                      );
                },
              //     (BuildContext context, int index) {
              //               final int itemIndex = index ~/ 2;

              //               if (index.isOdd){
              //                 return Padding(
              //                   padding: EdgeInsets.symmetric(vertical: 8.0),
              //                   child: Divider(height: 0, color: Colors.grey)
              //                 );
              //               }

              //               return itemIndex >= state.favourites.length
              //                   ? BottomLoader()
              //                   : Container(
              //                       child: Card(
              //                         // margin: const EdgeInsets.symmetric(vertical: 10.0),
              //                         shape: RoundedRectangleBorder(
              //                         borderRadius: BorderRadius.circular(10.0),
              //                       ),
              //                       elevation: 0.0,
              //                       child: InkWell(
              //                         child: RecommendationCard(recommendation: state.favourites[itemIndex]),
              //                         onTap: () {
              //                           Navigator.pushNamed(context, recommendation_details, arguments: state.favourites[itemIndex]);
              //                         },
              //                         ),
              //                       )
              //                   );
              //             }
              //   itemCount: state.hasReachedMax
              //       ? state.favourites.length
              //       : state.favourites.length + 1,
              //   controller: _scrollController,
              // )
              )
            )
          );
        }
        return null;
      },
    );
  }

  @override
  void dispose() {
    // Recommended to use bloc rather than using repository directly
    _favouriteRepository.refreshFavourite();
    _favouriteBloc.close();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _favouriteBloc.add(Fetch());
    }
  }
}