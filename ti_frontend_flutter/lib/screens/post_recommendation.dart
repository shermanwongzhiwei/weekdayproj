import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:ti_frontend_flutter/routes.dart';
import 'package:ti_frontend_flutter/post_recommendation_bloc/bloc.dart';
import 'package:ti_frontend_flutter/services/recommendation_repository.dart';
import 'package:ti_frontend_flutter/util/ui_helpers.dart';

class PostRecommendationPage extends StatefulWidget {
  final RecommendationRepository _recommendationRepository;
  final GlobalKey _bottomNavigationKey;

  PostRecommendationPage({Key key, RecommendationRepository recommendationRepository, GlobalKey bottomNavigationKey})
  : _recommendationRepository = recommendationRepository ?? RecommendationRepository(),
  _bottomNavigationKey = bottomNavigationKey,
  super(key: key);

  @override
  State<StatefulWidget> createState() => _PostRecommendationPageState();
}

class _PostRecommendationPageState extends State<PostRecommendationPage> {
  final _descriptionController = TextEditingController();
  final _restaurantController = TextEditingController();
  final _dishController = TextEditingController();
  String _restaurantErrorMessage;
  String _dishErrorMessage;
  List<Asset> _images = List<Asset>();
  PlacesSearchResult _selectedPlace;
  PostRecommendationBloc _postRecommendationBloc;

  BottomNavigationBar get _bottomNavigation => widget._bottomNavigationKey.currentWidget;
  RecommendationRepository get _recommendationRepository => widget._recommendationRepository;

  bool get _isPopulated => 
    _restaurantController.text.isNotEmpty &&
    _dishController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _postRecommendationBloc = PostRecommendationBloc(recommendationRepository: _recommendationRepository);
  }

  void _listener(BuildContext context, PostRecommendationState state) {
    if (state.isPosting) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Posting'),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    } else if (state.isSuccess) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Posted successfully'),
              Icon(Icons.check),
            ],
          ),
          backgroundColor: Colors.green,
        ),
      );
      _bottomNavigation.onTap(0);
      Navigator.pushNamed(context, recommendation_details, arguments: state.recommendation);
    } else if (state.isFailure) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Failed'),
              Icon(Icons.error),
            ],
          ),
          backgroundColor: Colors.red,
        ),
      );
    } else if(state.isRetrieveImagesFailure) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(state.retrieveImagesError),
              Icon(Icons.error),
            ],
          ),
        ),
      );
    }
  }

  void _onPost() {
    FocusScope.of(context).unfocus();
    setState(() {
      _dishErrorMessage = _dishController.text.isEmpty ? 'Dish cannot be left blank' : null;
      _restaurantErrorMessage = _restaurantController.text.isEmpty ? 'Restaurant cannot be left blank' : null;
    });
    if (_isPopulated) {
      _postRecommendationBloc.add(Post(
        description: _descriptionController.text,
        restaurant: _selectedPlace,
        dish: _dishController.text.toUpperCase(),
        images: _images,
      ));
    }
  }

  Widget _buildImagesGridView(BuildContext context, PostRecommendationState state) {
    if (state.images.length > 0) {
      _images = state.images;
      return Container(
        height: 200.0,        
        child: ListView.separated(
          // GridView.count(
          padding: const EdgeInsets.all(8),
          itemCount: state.images.length,
          separatorBuilder: (BuildContext context, int index) => const VerticalDivider(),
          scrollDirection : Axis.horizontal,
          // crossAxisSpacing: 10,
          // mainAxisSpacing: 10,
          // crossAxisCount: 3,
          itemBuilder: (BuildContext context, int index) {
            return Card(
                elevation: 3.0,
                child: AssetThumb(
                asset: _images[index],
                width: 200,
                height: 200,
                )
            );
          },
          // children: List.generate(_images.length, (index) => AssetThumb(
          //     asset: _images[index],
          //     width: 300,
          //     height: 300,
          //   ),
          // ),
        ),
      );
    } else {
      return Container(
        height: 0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PostRecommendationBloc>(
      create: (BuildContext context) => _postRecommendationBloc,
      child: BlocListener(
        bloc: _postRecommendationBloc,
        listener: _listener,
        child: Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            elevation: 0.0,
            centerTitle: false,
            backgroundColor: Colors.white,
            title: Text("Recommend", 
              style: TextStyle(
                fontFamily: 'Montserrat', 
                fontSize: 30.0,
                color: Theme.of(context).primaryColor, 
                fontWeight: FontWeight.bold)),
            actions: <Widget>[
              // FlatButton(
              //   textColor: Theme.of(context).primaryColor,
              //   onPressed: _onPost,
              //   child: Text('Post',
              //     style: TextStyle(
              //       fontFamily: 'Roboto',
              //       fontSize: 16.0,
              //     ),
              //   ),
              //   shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
              // ),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.all(15),
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Zeek'),
                      TextSpan(text: ' runs on '),
                      TextSpan(text: 'recommendations', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: ' instead of reviews. '),
                      TextSpan(text: 'Submit a restaurant only if you '),
                      TextSpan(text: 'recommend', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: ' it.'),

                    ]
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 20.0),
                ),
                // Container(
                //   padding: EdgeInsets.only(bottom: 0.0),
                //   child: Text('Restaurant', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Roboto', fontSize: 20, color: Colors.black))
                // ),
                Row(
                  children: <Widget>[
                      Expanded(
                        child: Padding(
                          // height: 45.0,
                          padding: EdgeInsets.only(right: 5.0),
                          child: TextFormField(
                            enabled: false,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'OpenSans',
                            ),
                            textInputAction: TextInputAction.done,
                            autocorrect: false,
                            autovalidate: true,
                            validator: (_) {
                              return _restaurantErrorMessage;
                            },
                            controller: _restaurantController,
                            decoration: InputDecoration(
                              disabledBorder: UnderlineInputBorder(

                                borderSide: BorderSide(color: Colors.white)
                              ),
                              // labelText: 'Restaurant',
                              labelText: 'Restaurant Name',
                              // filled: false,
                              // fillColor: Theme.of(context).dialogBackgroundColor,
                              // hintStyle: TextStyle(color: Color(0xff808080), fontFamily: 'Roboto'),
                              // enabledBorder: UnderlineInputBorder(
                              //   borderSide: BorderSide(width: 2.0, color: Color(0xff808080)),

                              // ),

                              // labelStyle: TextStyle(color: Colors.black26),
                              // enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black26)),
                              // labelText: 'Restaurant',
                              hintText: 'Village Park Restaurant',
                              // icon: Icon(
                              //   Icons.location_on,
                              //   color: Theme.of(context).primaryColor,
                              //   size: 30,
                              // ),
                            ),
                          ),
                        )
                      ),
                      SizedBox(
                        height: 50.0,
                        child: OutlineButton.icon(
                          borderSide: BorderSide(color: Theme.of(context).primaryColorLight),
                          color: Theme.of(context).primaryColorLight,
                          textColor: Theme.of(context).primaryColorLight,
                          label: Text("PICK A RESTAURANT"),
                          icon: Icon(
                            Icons.location_on,
                            color: Theme.of(context).primaryColorLight,
                            size: 30,
                          ),
                          onPressed: () async {
                            _selectedPlace = await Navigator.pushNamed<PlacesSearchResult>(context, add_location);
                            if (_selectedPlace != null) {
                              _restaurantController.text = _selectedPlace.name;
                            }
                          },
                        )
                      ),
                  ],
                ),
                // Container(
                //   padding: EdgeInsets.only(top: 15.0, bottom: 10.0),
                //   child: Text('Dish Name', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Montserrat', fontSize: 20))
                // ),
                UIHelpers.mBox,
                // Container(
                //   padding: EdgeInsets.only(bottom: 0.0),
                //   child: Text('Dish', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Roboto', fontSize: 20, color: Colors.black))
                // ),
                Container(
                    // height: 45.0,
                    child: TextFormField(
                      style: TextStyle(color: Colors.black),
                      enabled: true,
                      textInputAction: TextInputAction.next,
                      textCapitalization: TextCapitalization.words,
                      autocorrect: false,
                      autovalidate: true,
                      validator: (_) {
                        return _dishErrorMessage;
                      },
                      controller: _dishController,
                      decoration: InputDecoration(
                        labelText: 'Dish Name',
                        filled: false,
                        // fillColor: Theme.of(context).dialogBackgroundColor,
                        // hintStyle: TextStyle(color: Color(0xff808080), fontFamily: 'Roboto'),
                        // enabledBorder: UnderlineInputBorder(
                        //   borderSide: BorderSide(width: 2.0, color: Color(0xff808080)),

                        // ),
                        // enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black26)),
                        // border: OutlineInputBorder(),
                        // alignLabelWithHint: true,
                        // labelText: 'Dish',
                        hintText: 'Nasi Lemak',
                      ),
                    ),
                  ),
                // Container(
                //   padding: EdgeInsets.only(top: 15.0, bottom: 10.0),
                //   child: Text('Recommendation', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Montserrat', fontSize: 20))
                // ),
                UIHelpers.mBox,
                // Container(
                //   padding: EdgeInsets.only(bottom: 0.0),
                //   child: Text('Review', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Roboto', fontSize: 20, color: Colors.black))
                // ),
                TextFormField(
                  style: TextStyle(color: Colors.black),
                  textInputAction: TextInputAction.done,
                  textCapitalization: TextCapitalization.sentences,
                  keyboardType: TextInputType.multiline,
                  maxLines: 2,
                  autocorrect: false,
                  autovalidate: true,
                  controller: _descriptionController,
                  decoration: InputDecoration(
                    labelText: 'Review',
                    //   filled: false,
                    //   fillColor: Theme.of(context).dialogBackgroundColor,
                    //   hintStyle: TextStyle(color: Color(0xff808080), fontFamily: 'Roboto'),
                    //   enabledBorder: UnderlineInputBorder(
                    //     borderSide: BorderSide(width: 2.0, color: Color(0xff808080)),

                    //   ),
                    // // labelStyle: TextStyle(color: Colors.black26),
                    // // enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black26)),
                    // alignLabelWithHint: true,
                    // labelText: 'Positive Review',
                    hintText: 'Write a positive review here!',
                  ),
                ),
                UIHelpers.mBox,
                // Container(
                //   padding: EdgeInsets.only(bottom: 10.0),
                //   child: Text('   Upload Images', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Roboto', fontSize: 12, fontWeight: FontWeight.bold, color: Color(0xff808080)))
                // ),
                // ButtonBar(
                //   // buttonPadding: EdgeInsets.symmetric(horizontal: 20.0),
                //   buttonHeight: 40.0,
                //   children: <Widget>[
                      FlatButton.icon(
                        icon: Icon(Icons.insert_photo),
                        splashColor: Color(0x44F52545),
                        label: Text('ADD PHOTOS'),
                        // shape: RoundedRectangleBorder(
                        //   borderRadius: BorderRadius.circular(5.0),
                        // ),
                        // borderSide: BorderSide(
                        //     width: 1.0, color: Theme.of(context).primaryColor
                        // ),
                        // color: Theme.of(context).primaryColorLight,
                        // textColor: Colors.white,
                        onPressed: () {
                          print('Add images tapped');
                          _postRecommendationBloc.add(AddImages(currentImages: _images));
                        },
                        // child: Text(
                        //   "ADD IMAGES",
                        //   style: TextStyle(color: Theme.of(context).primaryColor)
                        // )
                    ),
                    // RaisedButton(
                    //   shape: RoundedRectangleBorder(
                    //     borderRadius: BorderRadius.circular(5.0),
                    //   ),
                    //   padding: EdgeInsets.symmetric(horizontal: 20.0),
                    //   onPressed: () => true,
                    //   textColor: Colors.white,
                    //   color: Theme.of(context).primaryColor,
                    //   // elevation: 1.0,
                    //   child: Text("POST"),
                    // ),
                //   ]
                // ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                ),
                UIHelpers.mBox,
                BlocBuilder<PostRecommendationBloc, PostRecommendationState>(
                  bloc: _postRecommendationBloc,
                  builder: _buildImagesGridView,
                ),
              ],
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Container(
              height: 70.0,
              width: double.infinity,
              child: Padding(
              padding: EdgeInsets.only(bottom: 20.0, left: 10.0, right: 10.0),
              child:  RaisedButton(
                elevation: 0.0,
                color: Color(0xff439C68),
                onPressed: _onPost,
                child: Text(
                    'SUBMIT',
                    style: TextStyle(),
                  ),
              ),
            )
          )
        ),
      ),
    );
  }

  @override
  void dispose() {
    _postRecommendationBloc.close();
    _descriptionController.dispose();
    _restaurantController.dispose();
    _dishController.dispose();
    super.dispose();
  }
}