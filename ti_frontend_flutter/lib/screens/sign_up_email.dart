import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:ti_frontend_flutter/controllers/validators.dart';
import 'package:ti_frontend_flutter/email_password_form_bloc/bloc.dart';
import 'package:ti_frontend_flutter/authentication_bloc/bloc.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';
import 'package:ti_frontend_flutter/util/ui_helpers.dart';

class SignUpEmail extends StatefulWidget {
  final UserRepository _userRepository;

  SignUpEmail({@required UserRepository userRepository}) : _userRepository = userRepository;

  @override
  State<StatefulWidget> createState() => _SignUpEmailState();
}

class _SignUpEmailState extends State<SignUpEmail> {
  EmailPasswordFormBloc formBloc;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final EmailValidator _emailValidator = Validators.getEmailValidator();
  final MultiValidator _passwordValidator = Validators.getPasswordValidator();
  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();
  bool isHidePassword = true;

  bool get isPopulated => emailController.text.isNotEmpty && passwordController.text.isNotEmpty;

  bool isSignUpButtonEnabled(EmailPasswordFormState state) {
    return _emailValidator.isValid(emailController.text) &&
        _passwordValidator.isValid(passwordController.text) &&
        isPopulated &&
        !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    formBloc = EmailPasswordFormBloc(
      userRepository: widget._userRepository,
    );
    emailController.addListener(_onEmailChanged);
    passwordController.addListener(_onPasswordChanged);
  }

  void _listener(BuildContext context, EmailPasswordFormState state) {
    logger.i('Listener sign up email');
    if (state.isSubmitting) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Signing Up'),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    } else if (state.isSuccess) {
      logger.i('Sign up success');
      BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
      Navigator.pop(context);
    } else if (state.isFailure) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Registration Failure'),
              Icon(Icons.error),
            ],
          ),
          backgroundColor: Colors.red,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          // title: Text('Sign Up',
          //   style: TextStyle(
          //     fontFamily: 'Montserrat',
          //     fontWeight: FontWeight.bold,

          //   ),
          // ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black
          )
        ),
        body: BlocProvider<EmailPasswordFormBloc>(
          create: (BuildContext _) => formBloc,
          child: BlocListener<EmailPasswordFormBloc, EmailPasswordFormState>(
            listener: _listener,
            child: BlocBuilder<EmailPasswordFormBloc, EmailPasswordFormState>(
              builder: (BuildContext c, EmailPasswordFormState state) {
                logger.d('Rebuilding Sign Up Email');
                return Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom : 20.0),
                        child: Text("Sign Up",
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontFamily: 'Montserrat',
                            fontSize: 25.0,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        child: TextFormField(
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.emailAddress,
                          autocorrect: false,
                          autovalidate: true,
                          validator: _emailValidator,
                          controller: emailController,
                          onFieldSubmitted: (_) {
                            emailFocusNode.unfocus();
                            FocusScope.of(context).requestFocus(passwordFocusNode);
                          },
                          decoration: InputDecoration(
                            labelText: 'Email',
                          ),
                        ),
                      ),
                      UIHelpers.mBox,
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal : 20.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: TextFormField(
                                textInputAction: TextInputAction.done,
                                keyboardType: TextInputType.visiblePassword,
                                focusNode: passwordFocusNode,
                                controller: passwordController,
                                autocorrect: false,
                                autovalidate: true,
                                validator: _passwordValidator,
                                obscureText: isHidePassword,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  labelText: 'Password',
                                ),
                              ),
                            ),
                            IconButton(
                              icon: Icon(
                                isHidePassword ? Icons.visibility_off : Icons.visibility,
                              ),
                              onPressed: () {
                                setState(() {
                                  isHidePassword = !isHidePassword;
                                });
                              },
                            )
                          ],
                        ),
                      ),

                      UIHelpers.mBox,
                      SizedBox(
                        height: 50.0,
                        width: 320.0,
                        child: FlatButton(
                          disabledColor: Theme.of(context).primaryColor,
                          onPressed: isSignUpButtonEnabled(state) ? _onFormSubmitted : null,
                          color: Theme.of(context).primaryColor,
                          child: Text(
                            'Sign Up',
                            style: TextStyle(
                              color: Colors.white
                            )
                          ),
                        ),
                      ),

                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    emailController.dispose();
    passwordController.dispose();
    formBloc.close();
    super.dispose();
  }

  void _onEmailChanged() {
    formBloc.add(
      FormEmailChanged(email: emailController.text),
    );
  }

  void _onPasswordChanged() {
    formBloc.add(
      FormPasswordChanged(password: passwordController.text),
    );
  }

  void _onFormSubmitted() {
    logger.d('Form submitted');
    FocusScope.of(context).unfocus();
    formBloc.add(
      SignUpFormSubmitted(
        email: emailController.text,
        password: passwordController.text,
      ),
    );
  }
}
