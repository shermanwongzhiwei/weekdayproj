import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:ti_frontend_flutter/controllers/validators.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';
import 'package:ti_frontend_flutter/util/ui_helpers.dart';
import 'package:ti_frontend_flutter/email_password_form_bloc/bloc.dart';
import 'package:ti_frontend_flutter/authentication_bloc/bloc.dart';

class SignInEmail extends StatefulWidget {
  final UserRepository _userRepository;

  SignInEmail({@required UserRepository userRepository}) : _userRepository = userRepository;

  @override
  State<StatefulWidget> createState() => _SignInEmailState();
}

class _SignInEmailState extends State<SignInEmail> {
  EmailPasswordFormBloc formBloc;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final EmailValidator _emailValidator = Validators.getEmailValidator();
  final MultiValidator _passwordValidator = Validators.getPasswordValidator();
  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();
  bool isHidePassword = true;

  bool get isPopulated => emailController.text.isNotEmpty && passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    formBloc = EmailPasswordFormBloc(userRepository: widget._userRepository);
    emailController.addListener(_onEmailChanged);
    passwordController.addListener(_onPasswordChanged);
  }

  bool isSignInButtonEnabled(EmailPasswordFormState state) {
    logger.d(state);
    return _emailValidator.isValid(emailController.text) &&
        _passwordValidator.isValid(passwordController.text) &&
        isPopulated &&
        !state.isSubmitting;
  }

  void _signInListener(BuildContext context, EmailPasswordFormState state) {
    if (state.isFailure) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text('Login Failure'), Icon(Icons.error)],
          ),
          backgroundColor: Colors.red,
        ),
      );
    }
    if (state.isSubmitting) {
      Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Logging In...'),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    }
    if (state.isSuccess) {
      BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Login'),
        elevation: 0,
        iconTheme: IconThemeData(
          color: Color(0xFF757575)
        ),
        textTheme: TextTheme(
            title: TextStyle(fontSize: 14.0, fontFamily: 'Roboto', color: Color(0xFF757575), fontWeight: FontWeight.bold)
            ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: BlocProvider<EmailPasswordFormBloc>(
        create: (BuildContext context) => formBloc,
        child: BlocListener(
          bloc: formBloc,
          listener: _signInListener,
          child: BlocBuilder<EmailPasswordFormBloc, EmailPasswordFormState>(
            builder: (BuildContext context, EmailPasswordFormState state) {
              return Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                      autocorrect: false,
                      autovalidate: true,
                      validator: _emailValidator,
                      controller: emailController,
                      onFieldSubmitted: (_) {
                        emailFocusNode.unfocus();
                        FocusScope.of(context).requestFocus(passwordFocusNode);
                      },
                      decoration: InputDecoration(
                        labelText: 'Email',
                      ),
                    ),
                    UIHelpers.mBox,
                    TextFormField(
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.visiblePassword,
                      focusNode: passwordFocusNode,
                      controller: passwordController,
                      autocorrect: false,
                      autovalidate: true,
                      validator: _passwordValidator,
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'Password',
                      ),
                    ),
                    UIHelpers.mBox,
                    SizedBox(
                      width: double.infinity,
                      height: 50.0,
                      child: FlatButton(
                        textColor: Colors.white,
                        disabledColor: Theme.of(context).primaryColor,
                        disabledTextColor: Colors.white,
                        splashColor: Colors.redAccent,
                        onPressed: isSignInButtonEnabled(state) ? _onFormSubmitted : null,
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          'Sign In',
                          style: TextStyle(color: Colors.white, fontFamily: 'Roboto'),
                        ),
                      ),
                    )

                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    formBloc.close();
    super.dispose();
  }

  void _onEmailChanged() {
    formBloc.add(
      FormEmailChanged(email: emailController.text),
    );
  }

  void _onPasswordChanged() {
    logger.i(passwordController.text);
    formBloc.add(
      FormPasswordChanged(password: passwordController.text),
    );
  }

  void _onFormSubmitted() {
    FocusScope.of(context).unfocus();
    formBloc.add(
      SignInFormSubmitted(
        email: emailController.text,
        password: passwordController.text,
      ),
    );
  }
}
