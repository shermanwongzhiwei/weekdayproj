import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:ti_frontend_flutter/models/favourite.dart';
import 'package:ti_frontend_flutter/models/dislike.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:ti_frontend_flutter/services/dislike_repository.dart';
import 'package:ti_frontend_flutter/services/favourite_repository.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:ti_frontend_flutter/util/logging.dart';
import 'package:recase/recase.dart';
import 'package:share/share.dart';

class RecommendationDetails extends StatefulWidget {
  final Recommendation _recommendation;
  final FavouriteRepository _favouriteRepository;
  final DislikeRepository _dislikeRepository;

  RecommendationDetails({
    Key key, 
    Recommendation recommendation, 
    FavouriteRepository favouriteRepository,
    DislikeRepository dislikeRepository,
  }): _recommendation = recommendation,
      _favouriteRepository = favouriteRepository ?? FavouriteRepository(),
      _dislikeRepository = dislikeRepository ?? DislikeRepository(),
      super(key: key);

  @override
  State<StatefulWidget> createState() => _RecommendationDetails();
}

class _RecommendationDetails extends State<RecommendationDetails> {

  Future<Favourite> _favourite;
  Future<Dislike> _dislike;
  bool isFaved;
  bool hasDisliked;

  Recommendation get _recommendation => widget._recommendation;
  FavouriteRepository get _favouriteRepository => widget._favouriteRepository;
  DislikeRepository get _dislikeRepository => widget._dislikeRepository;

  @override
  void initState() {
    super.initState();
    _favourite = _favouriteRepository.getFavouriteStatus(recommendationId: _recommendation.id);
    this.isFaved = false;
    _favourite.then((favourite){
      this.isFaved = favourite.hasSavedAsFavourite;
    });

    _dislike = _dislikeRepository.getDislikeStatus(recommendationId: _recommendation.id);
    this.hasDisliked = false;
    _dislike.then((dislike) {
      this.hasDisliked = dislike.hasSavedAsDislike;
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.black87,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text('', style: TextStyle(color: Colors.black)),
      elevation: 0.0,
    ), 
  
    body: SafeArea(
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(bottom:15.0),
                    child: Text(
                      (new ReCase('${_recommendation.dish}')).titleCase,
                      textAlign: TextAlign.center,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Center(
                    // padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right:5.0),
                          child: Icon(
                            Icons.location_on,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        Flexible(
                          child: Text(
                          '${_recommendation.restaurant}',
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Open Sans',
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  
                  Container(
                    height: (_recommendation.pictures.length > 0) ? MediaQuery.of(context).size.width * 0.9 : 20.0,
                    color: Colors.transparent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Swiper(
                        itemBuilder: (BuildContext context, int index) {
                          return ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.network(
                              _recommendation.pictures[index],
                              fit: BoxFit.contain,
                            ),
                          );
                        },
                        itemCount: _recommendation.pictures.length,
                        viewportFraction: 0.9,
                        scale: 0.9,
                        loop: _recommendation.pictures.length > 1,
                        pagination: _recommendation.pictures.length > 1 ? SwiperPagination() : null,
                      ),
                    ),
                  ),

                  (_recommendation.description != null) ?

                  Column(
                  children: <Widget>[Divider(
                    color: Color(0xff707070),
                    thickness: 1,
                    indent: 35,
                    endIndent: 35,
                  ),
                  Container(
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 40.0),
                      child: Text(
                        '"${_recommendation.description}"',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          height: 1.3,
                          fontFamily: 'Open Sans',
                          fontStyle: FontStyle.italic,
                          fontSize: 18.0,
                          color: Color(0xff333333),
                        ),
                      ),
                    ),
                  ),

                  Divider(
                    color: Color(0xff707070),
                    thickness: 1,
                    indent: 35,
                    endIndent: 35,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: null,
                  ),]

                  ) : Container(),

                  Text(
                    'Posted on February 2, 2020, at 2.36 p.m. by',
                    style: TextStyle(
                      color: Color(0xff8C8C8C),
                      fontFamily: 'Open Sans',
                      fontSize: 12.0,                      
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15,
                          child: Icon(
                            Icons.person,
                            size: 15,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            '${_recommendation.name}',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 16.0,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 30.0),
                    child: Wrap(
                      alignment: WrapAlignment.start,
                      // spacing: 4.0,
                      runSpacing: 4.0,
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0.0),
                          child: FutureBuilder(
                            future: _favourite,
                            builder: (BuildContext context, AsyncSnapshot<Favourite> snapshot) {
                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.0),
                                child: Column(
                                children: 
                                    (this.isFaved) ? 
                                  <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 0.0),
                                      child: IconButton(
                                        onPressed: (){
                                          _favouriteRepository.removeFromFavourite(recommendationId: _recommendation.id);
                                          this.isFaved = false;
                                          setState(() {
                                            _favourite = _favouriteRepository.getFavouriteStatus(recommendationId: _recommendation.id);
                                          });  
                                        },
                                        icon: Icon(
                                          Icons.favorite,
                                          color: Theme.of(context).primaryColor,
                                          size: 30.0,
                                        ),
                                      ),
                                    ),
                                    Text('FAVED!',
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400
                                      )
                                    )
                                  ] :
                                  <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 0.0),
                                      child: IconButton(
                                        onPressed: (){
                                          _favouriteRepository.saveAsFavourite(recommendationId: _recommendation.id);
                                          this.isFaved = true;
                                          setState(() {
                                            _favourite = _favouriteRepository.getFavouriteStatus(recommendationId: _recommendation.id);
                                          });                                      
                                        },
                                        icon: Icon(
                                          Icons.favorite_border,
                                          color: Color(0xff757575),
                                          size: 30.0,
                                        ),
                                      ),
                                    ),
                                    Text('FAVORITE',
                                      style: TextStyle(
                                        color: Color(0xff757575),
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400
                                      )
                                    )
                                  ],                          
                                ),
                              );
                              }
                            )
                        ),


                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0.0),
                          child: FutureBuilder(
                            future: _dislike,
                            builder: (BuildContext context, AsyncSnapshot<Dislike> snapshot) {
                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.0),
                                child: Column(
                                children: 
                                  (
                                    snapshot.connectionState == ConnectionState.done && 
                                    !snapshot.hasError &&
                                    snapshot.data.hasSavedAsDislike
                                  ) ? 
                                  <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 0.0),
                                      child: IconButton(
                                        onPressed: () {
                                          _dislikeRepository.removeFromDislike(recommendationId: _recommendation.id);
                                          this.hasDisliked = false;
                                          setState(() {
                                            _dislike = _dislikeRepository.getDislikeStatus(recommendationId: _recommendation.id);
                                          });  
                                        },
                                        icon: Icon(
                                          Icons.thumb_down,
                                          color: Colors.black,
                                          size: 30.0,
                                        ),
                                      ),
                                    ),
                                    Text('DISLIKED!',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400
                                      )
                                    )
                                  ] :
                                  <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 0.0),
                                      child: IconButton(
                                        onPressed: (){
                                          _dislikeRepository.saveAsDislike(recommendationId: _recommendation.id);
                                          this.hasDisliked = true;
                                          setState(() {
                                            _dislike = _dislikeRepository.getDislikeStatus(recommendationId: _recommendation.id);
                                          });                                      
                                        },
                                        icon: Icon(
                                          OMIcons.thumbDown,
                                          color: Color(0xff757575),
                                          size: 30.0,
                                        ),
                                      ),
                                    ),
                                    Text('DISLIKE DISH',
                                      style: TextStyle(
                                        color: Color(0xff757575),
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400
                                      )
                                    )
                                  ],                          
                                ),
                              );


                              }
                            )
                        ),


                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0.0),
                          child: FutureBuilder(
                            future: _favourite,
                            builder: (BuildContext context, AsyncSnapshot<Favourite> snapshot) {
                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.0),
                                child: Column(
                                children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 0.0),
                                      child: IconButton(
                                        onPressed: _launchURL,
                                        icon: Icon(
                                          OMIcons.locationSearching,
                                          color: Color(0xff757575),
                                          size: 30.0,
                                        ),
                                      )
                                    ),
                                    Text('LOCATION',
                                      style: TextStyle(
                                        color: Color(0xff757575),
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400
                                      )
                                    )
                                  ],                          
                                ),
                              );

                              
                              }
                            )
                        ),


                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0.0),
                          child: FutureBuilder(
                            future: _favourite,
                            builder: (BuildContext context, AsyncSnapshot<Favourite> snapshot) {
                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.0),
                                child: Column(
                                children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 0.0),
                                      child: IconButton(
                                        onPressed: (){Share.share("Found this recommendation on Zeek : ${_recommendation.dish} @ ${_recommendation.restaurant} - ${_recommendation.googleLink}");},
                                        icon: Icon(
                                          OMIcons.share,
                                          color: Color(0xff757575),
                                          size: 30.0,
                                        ),
                                      )
                                    ),
                                    Text('SHARE',
                                      style: TextStyle(
                                        color: Color(0xff757575),
                                        fontFamily: 'Roboto',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w400
                                      )
                                    )
                                  ],                          
                                ),
                              );

                            
                              }
                            )
                        ),
                      ],
                    ),
                  ),
                  

                  // Container(
                  //   child: ToggleButtons(
                  //     renderBorder: false,
                  //     splashColor: Colors.transparent,
                  //     fillColor: Colors.white,
                  //     isSelected: isSelected,
                  //     onPressed: (int index) {
                  //       setState(() {
                  //         for (int buttonIndex = 0; buttonIndex < isSelected.length; buttonIndex++) {
                  //           if (buttonIndex == index) {
                  //             isSelected[buttonIndex] = !isSelected[buttonIndex];
                  //           }
                  //         }
                  //       });
                  //     },
                  //     children: <Widget>[
                  //       Padding(
                  //         padding: EdgeInsets.symmetric(horizontal: 15.0),
                  //         child: Column(
                  //         children: isSelected[0] ? 
                  //           <Widget>[
                  //           Padding(
                  //             padding: EdgeInsets.only(bottom: 4.0),
                  //             child: Icon(
                  //               Icons.favorite,
                  //               color: Theme.of(context).primaryColor,
                  //               size: 30.0,
                  //             ),
                  //           ),
                  //           Text('FAVED!',
                  //             style: TextStyle(
                  //               color: Theme.of(context).primaryColor,
                  //               fontFamily: 'Roboto',
                  //               fontSize: 14.0,
                  //               fontWeight: FontWeight.w400
                  //             )
                  //           )
                  //           ]
                  //           : <Widget>[
                  //           Padding(
                  //             padding: EdgeInsets.only(bottom: 4.0),
                  //             child: Icon(
                  //               Icons.favorite_border,
                  //               color: Colors.black,
                  //               size: 30.0,
                  //             ),
                  //           ),
                  //           Text('FAVORITE',
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontFamily: 'Roboto',
                  //               fontSize: 14.0,
                  //               fontWeight: FontWeight.w400
                  //             )
                  //           )
                  //         ],
                  //       ), 
                  //       ), 
                  //       Padding(
                  //         padding: EdgeInsets.symmetric(horizontal: 15.0),
                  //         child: Column(
                  //         children: isSelected[1] ? 
                  //         <Widget>[
                  //           Padding(
                  //             padding: EdgeInsets.only(bottom: 4.0),
                  //             child: Icon(
                  //               Icons.thumb_down,
                  //               color: Colors.black,
                  //               size: 30.0,
                  //             ),
                  //           ),
                  //           Text('DISLIKED',
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontFamily: 'Roboto',
                  //               fontSize: 14.0,
                  //               fontWeight: FontWeight.w400
                  //             )
                  //           )
                  //         ]
                  //         :
                  //         <Widget>[
                  //           Padding(
                  //             padding: EdgeInsets.only(bottom: 4.0),
                  //             child: Icon(
                  //               OMIcons.thumbDown,
                  //               color: Colors.black,
                  //               size: 30.0,
                  //             ),
                  //           ),
                  //           Text('DISLIKE',
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontFamily: 'Roboto',
                  //               fontSize: 14.0,
                  //               fontWeight: FontWeight.w400
                  //             )
                  //           )
                  //         ],
                  //       ), 
                  //       ),
                  //       Padding(
                  //         padding: EdgeInsets.symmetric(horizontal: 15.0),
                  //         child: Column(
                  //         children: <Widget>[
                  //           Padding(
                  //             padding: EdgeInsets.only(bottom: 4.0),
                  //             child: Icon(
                  //               Icons.favorite_border,
                  //               color: Colors.black,
                  //               size: 30.0,
                  //             ),
                  //           ),
                  //           Text('FAVORITE',
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontFamily: 'Roboto',
                  //               fontSize: 14.0,
                  //               fontWeight: FontWeight.w400
                  //             )
                  //           )
                  //         ],
                  //       ), 
                  //       ),
                  //       Padding(
                  //         padding: EdgeInsets.symmetric(horizontal: 15.0),
                  //         child: Column(
                  //         children: <Widget>[
                  //           Padding(
                  //             padding: EdgeInsets.only(bottom: 4.0),
                  //             child: Icon(
                  //               Icons.favorite_border,
                  //               color: Colors.black,
                  //               size: 30.0,
                  //             ),
                  //           ),
                  //           Text('FAVORITE',
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontFamily: 'Roboto',
                  //               fontSize: 14.0,
                  //               fontWeight: FontWeight.w400
                  //             )
                  //           )
                  //         ],
                  //       ), 
                  //       ),

                  //     ],
                  //   ),
                  // ),                    

                            // return RaisedButton.icon(
                            //   icon: Icon(
                            //     Icons.favorite,
                            //     color: Colors.white,
                            //   ),
                            //   label: 
                            //   Text(
                            //     'Save to Favourites',
                            //     style: TextStyle(
                            //       color: Colors.white,
                            //       fontWeight: FontWeight.w500
                            //     ),
                            //   ),
                            //   autofocus: false,
                            //   clipBehavior: Clip.none,
                            //   color: Theme.of(context).primaryColor,
                              // onPressed: () {
                              //   _favouriteRepository.saveAsFavourite(recommendationId: _recommendation.id);
                              //   setState(() {
                              //     _favourite = _favouriteRepository.getFavouriteStatus(recommendationId: _recommendation.id);
                              //   });
                              // },
                  // Padding(
                  //   padding: EdgeInsets.all(10),
                  //   child: ButtonTheme(
                  //     minWidth: 200,
                  //     height: 45,
                  //     child: RaisedButton.icon(
                  //       icon: Icon(
                  //         Icons.clear,
                  //         color: Colors.black87,
                  //       ),
                  //       label: Text(
                  //         'Don\'t Like This Dish',
                  //         style: TextStyle(
                  //           color: Colors.black87,
                  //           fontWeight: FontWeight.w500
                  //         ),
                  //       ),
                  //       autofocus: false,
                  //       clipBehavior: Clip.none,
                  //       color: Colors.white,
                  //       onPressed: () {},
                  //     ),
                  //   ),
                  // ),
                  // Padding(
                  //   padding: EdgeInsets.all(10),
                  //   child: ButtonTheme(
                  //     minWidth: 200,
                  //     height: 45,
                  //     child: RaisedButton.icon(
                  //       icon: Icon(
                  //         Icons.share,
                  //         color: Colors.white,
                  //       ),
                  //       label: Text(
                  //         'Share',
                  //         style: TextStyle(
                  //           color: Colors.white,
                  //           fontWeight: FontWeight.w500
                  //         ),
                  //       ),
                  //       autofocus: false,
                  //       clipBehavior: Clip.none,
                  //       color: Theme.of(context).primaryColorLight,
                  //       onPressed: () {},
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
        ),
      ),
    ),
    // floatingActionButton: FloatingActionButton(
    //   child: Icon(Icons.location_on),
    //   onPressed: _launchURL,
    // ),
  );

  _launchURL() async {
    if (await canLaunch(_recommendation.googleLink)) {
      logger.v('Launch ${_recommendation.googleLink}');
      await launch(_recommendation.googleLink);
    } else {
      throw 'Could not launch ${_recommendation.googleLink}';
    }
  }
}
