import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:ti_frontend_flutter/screens/home.dart';
import 'package:ti_frontend_flutter/screens/favourite.dart';
import 'package:ti_frontend_flutter/screens/post_recommendation.dart';
import 'package:ti_frontend_flutter/screens/profile.dart';
import 'package:ti_frontend_flutter/models/user.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

class MainBottomNavigation extends StatefulWidget {
  final User _currentUser;

  MainBottomNavigation({Key key, @required User currentUser})
    : _currentUser = currentUser,
      super(key: key);

  @override
  _MainBottomNavigationState createState() => _MainBottomNavigationState();
}

class _MainBottomNavigationState extends State<MainBottomNavigation> {
  int _selectedIndex = 0;
  GlobalKey bottomNavigationKey = new GlobalKey(debugLabel: 'bottom_navigation_bar');
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        logger.v('onMessage: $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        logger.v("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        logger.v("onResume: $message");
      },
    );

    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      logger.v('Firebase Messaging Token: $token');
    });

    _firebaseMessaging.subscribeToTopic('recommendation');
  }

  List<Widget> _widgetOptions() => <Widget>[
    Home(),
    PostRecommendationPage(bottomNavigationKey: bottomNavigationKey,),
    FavouritePage(),
    ProfilePage(currentUser: widget._currentUser),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> widgetOptions = _widgetOptions();

    return Scaffold(
      body: SafeArea(
        child: widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        key: bottomNavigationKey,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home',
              style: TextStyle(fontFamily: 'Montserrat', fontSize: 12.0)
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            title: Text('Recommend',
              style: TextStyle(fontFamily: 'Montserrat', fontSize: 12.0)
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text('Favourites',
              style: TextStyle(fontFamily: 'Montserrat', fontSize: 12.0)
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile',
              style: TextStyle(fontFamily: 'Montserrat', fontSize: 12.0)
            ),
          ),
        ],
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Theme.of(context).accentColor,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
        showSelectedLabels: true,
        showUnselectedLabels: true,
      ),
    );
  }
}
