import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart';
import 'package:ti_frontend_flutter/home_feed_bloc/bloc.dart';
import 'package:ti_frontend_flutter/models/location_result.dart';
import 'package:ti_frontend_flutter/routes.dart';
import 'package:ti_frontend_flutter/services/recommendation_repository.dart';
import 'package:ti_frontend_flutter/widgets/bottom_loader.dart';
import 'package:ti_frontend_flutter/widgets/recommendation_card.dart';

class Home extends StatefulWidget {
  final RecommendationRepository _recommendationRepository;

  Home({Key key, RecommendationRepository recommendationRepository})
    : _recommendationRepository = recommendationRepository ?? RecommendationRepository(),
    super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  // final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  HomeFeedBloc _homeFeedBloc;
  Completer<void> _refreshCompleter;

  RecommendationRepository get _recommendationRepository => widget._recommendationRepository;
  

  @override
  void initState() {
    super.initState();
    _homeFeedBloc = HomeFeedBloc(recommendationRepository: _recommendationRepository);
    _homeFeedBloc.add(Fetch());
    _scrollController.addListener(_onScroll);
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      listener: (context, state){
        if (state is HomeFeedLoaded){
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();

          if (!(state.locationResult is CurrentLocation)) {
            String msg;
            if (state.locationResult is LastKnownLocation) {
              msg = 'Using last known location';
            } else if (state.locationResult is NoLocation) {
              msg = 'Let us know your location';
            }

            Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Text(msg),
                duration: Duration(seconds: 20),
                action: SnackBarAction(
                  label: 'Enable GPS',
                  onPressed: () async {
                    final result = await Location().requestService();
                    if (result) {
                      _homeFeedBloc.add(Refresh());
                    }
                  },
                ),
              ),
            );
          }
        }
      },
      bloc: _homeFeedBloc,
      builder: (BuildContext context, HomeFeedState state) {
        if (state is HomeFeedUninitialized) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              // flexibleSpace: Placeholder(),
              title: Text(' zeek',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
              )
            ),
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        if (state is HomeFeedError) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              // flexibleSpace: Placeholder(),
              title: Text(' zeek',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
              )
            ),
            backgroundColor: Colors.white,
            body: Center(
              child: Text('Failed to fetch recommendations'),
            )
          );
        }
        if (state is HomeFeedLoaded) {
          if (state.recommendations.isEmpty) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 0.0,
                // flexibleSpace: Placeholder(),
                title: Text(' zeek',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30.0,
                  ),
                )
              ), 
              body: Center(
                child: Text('No recommendations'),
              )
            );
          }
            return Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: RefreshIndicator(
                // key: _refreshIndicatorKey,
                onRefresh: (){
                  _homeFeedBloc.add(Refresh());
                  return _refreshCompleter.future;
                },
                child: CustomScrollView(
                  controller: _scrollController,
                  slivers: <Widget>[
                    SliverAppBar(
                      backgroundColor: Colors.white,
                      elevation: 0.0,
                      floating: true,
                      // flexibleSpace: Placeholder(),
                      title: Text('zeek',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 30.0,
                        ),
                      )
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            final int itemIndex = index ~/ 2;

                            if (index.isOdd){
                              return Padding(
                                padding: EdgeInsets.symmetric(vertical: 8.0),
                                child: Divider(height: 0, color: Colors.grey)
                              );
                            }

                            if (state.hasReachedMax && itemIndex >= state.recommendations.length){
                              return Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10.0),
                                  child: Text(
                                    "You've reached the end! Help us by contributing your recommendations today.",
                                    textAlign: TextAlign.center,
                                  )
                                )
                              );
                            }

                            return itemIndex >= state.recommendations.length
                                ? BottomLoader()
                                : Container(
                                    child: Card(
                                      // margin: const EdgeInsets.symmetric(vertical: 10.0),
                                      shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    elevation: 0.0,
                                    child: InkWell(
                                      child: RecommendationCard(recommendation: state.recommendations[itemIndex]),
                                      onTap: () {
                                        Navigator.pushNamed(context, recommendation_details, arguments: state.recommendations[itemIndex]);
                                      },
                                      ),
                                    )
                                );
                          },
                          childCount: state.hasReachedMax 
                            ? state.recommendations.length * 2 + 1
                            : state.recommendations.length * 2
                  //       itemCount: state.hasReachedMax
                  //           ? state.recommendations.length
                  //           : state.recommendations.length + 1
                        )
                      ),
                  ],
                  // body: Container(
                  //   color: Colors.white,
                  //   child: Padding(
                  //     padding: const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                  //     child: ListView.builder(
                  //       itemBuilder: (BuildContext context, int index) {
                  //         return index >= state.recommendations.length
                  //             ? BottomLoader()
                  //             : Container(
                  //                 child: Card(
                  //                   margin: const EdgeInsets.symmetric(vertical: 10.0),
                  //                   shape: RoundedRectangleBorder(
                  //                   borderRadius: BorderRadius.circular(10.0),
                  //                 ),
                  //                 elevation: 0.0,
                  //                 child: InkWell(
                  //                   child: RecommendationCard(recommendation: state.recommendations[index]),
                  //                   onTap: () {
                  //                     Navigator.pushNamed(context, recommendation_details, arguments: state.recommendations[index]);
                  //                   },
                  //                   ),
                  //                 )
                  //             );
                  //       },
                  //       itemCount: state.hasReachedMax
                  //           ? state.recommendations.length
                  //           : state.recommendations.length + 1,
                  //       controller: _scrollController,
                  //     )
                  //   )
                  // )

                )
            )
          );
        }
        return null;  // Not reachable
      },
    );
  }

  @override
  void dispose() {
    // Recommended to use bloc rather than using repository directly
    _recommendationRepository.refreshRecommendations();
    _homeFeedBloc.close();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _homeFeedBloc.add(Fetch());
    }
  }
}