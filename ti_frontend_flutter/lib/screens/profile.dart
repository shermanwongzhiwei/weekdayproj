import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ti_frontend_flutter/authentication_bloc/bloc.dart';
import 'package:ti_frontend_flutter/models/user.dart';


class ProfilePage extends StatefulWidget {
  final User _currentUser;

  ProfilePage({Key key, @required User currentUser})
    : _currentUser = currentUser,
      super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  User get _user => widget._currentUser;

  Widget _profilePicture() => _user.profilePicture != null
    ? CircleAvatar(
      backgroundImage: NetworkImage(_user.profilePicture),
      radius: 40,
    ) : CircleAvatar(
      child: Icon(
        Icons.person,
        size: 40,
      ),
      radius: 40,
    );

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      elevation: 0.0,
      title: Text('Profile',
        style: TextStyle(
          fontFamily: 'Montserrat',
          color: Theme.of(context).primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 30.0,
        ),
      ),
      backgroundColor: Colors.white,
      actions: <Widget>[
        // IconButton(
        //   icon: Icon(Icons.exit_to_app),
        //   color: Colors.black,
        //   onPressed: () {
        //     BlocProvider.of<AuthenticationBloc>(context).add(
        //       LoggedOut(),
        //     );
        //   },
        // )
        FlatButton(
          textColor: Theme.of(context).primaryColorLight,
          child: Text('Logout',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16.0,
            ),
          ),
          // color: Colors.black,
          onPressed: () {
            BlocProvider.of<AuthenticationBloc>(context).add(
              LoggedOut(),
            );
          },
        ),
      ],
    ),
    body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 15, left: 15),
              child: _profilePicture(),
            ),
            Expanded(
              child: Container(
                height: 80,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('${_user.name}',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: Colors.black
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Text('${_user.followers}',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            Text('Followers',
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Text('${_user.following}',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            Text('Following',
                              style: TextStyle(
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Divider(),
        Text(
          "Currently under construction. Check back soon!",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontFamily: 'Montserrat',
            fontSize: 20.0,
          )
        ),
      ],
    ),
  );
}