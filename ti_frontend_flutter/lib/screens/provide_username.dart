import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ti_frontend_flutter/authentication_bloc/bloc.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/util/ui_helpers.dart';


class ProvideUsernamePage extends StatefulWidget {
  final UserRepository _userRepository;

  ProvideUsernamePage({Key key, UserRepository userRepository})
    : _userRepository = userRepository ?? UserRepository(),
      super(key: key);

  @override
  State<StatefulWidget> createState() => _ProvideUsernamePageState();
}

class _ProvideUsernamePageState extends State<ProvideUsernamePage> {
  TextEditingController _usernameController = new TextEditingController();
  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext _) => Scaffold(
    body: Builder(
      builder: (BuildContext context) => SafeArea(
        child: Padding(
          padding: EdgeInsets.all(UIHelpers.padding_medium),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("What's your name?",
                style: TextStyle(
                  color: Color(0xff333333),
                  fontFamily: 'Montserrat',
                  fontSize: 25.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20.0, top: 20.0, left: 20.0, right: 20.0),
                child: TextFormField(
                  textCapitalization: TextCapitalization.words,
                  controller: _usernameController,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  autocorrect: false,
                  autovalidate: false,
                  maxLength: 20,
                  decoration: InputDecoration(
                    // border: OutlineInputBorder(),
                    labelText: 'Pick a name to show the world!',
                    labelStyle: TextStyle(
                      fontFamily: 'OpenSans',
                    ),
                  ),
                ),
              ),
              UIHelpers.sBox,
              SizedBox(
                height: 50.0,
                width: 320.0,
                child: RaisedButton(
                  child: Text('Continue',
                    style: TextStyle(
                      color: Colors.white,
                    )
                  ),
                  color: Theme.of(context).primaryColor,
                  onPressed: () async {
                    Scaffold.of(context)
                      ..hideCurrentSnackBar()
                      ..showSnackBar(
                      SnackBar(
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Logging In...'),
                            CircularProgressIndicator(),
                          ],
                        ),
                      ),
                    );
                    await _userRepository.initializeUser(_usernameController.text);
                    BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}