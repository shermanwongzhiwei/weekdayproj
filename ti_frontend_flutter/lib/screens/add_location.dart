import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ti_frontend_flutter/add_location_bloc/bloc.dart';


class AddLocationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddLocationPageState();
}

class _AddLocationPageState extends State<AddLocationPage> {
  AddLocationBloc _addLocationBloc;
  TextEditingController _searchQueryController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _addLocationBloc = AddLocationBloc();
  }

  @override
  void dispose() {
    _addLocationBloc.close();
    _searchQueryController.dispose();
    super.dispose();
  }

  Widget _searchSuggestions(BuildContext context, AddLocationState state) {
    return Container(
      color: Colors.white,
      height: double.infinity,
      width: double.infinity,
      child: state.isSearching && !state.onTap
        ? ListView.separated(
          itemCount: state.suggestions.length,
          itemBuilder: (_, index) => ListTile(
            title: Text(
              state.suggestions[index].name,
              maxLines: 1,
            ),
            subtitle: Text(
              state.suggestions[index].formattedAddress,
              maxLines: 1,
            ),
            onTap: () {
              _addLocationBloc.add(SuggestionTapped(suggestion: state.suggestions[index]));
            },
          ),
          separatorBuilder: (_, index) {
            return Divider();
          },
        )
        : null,
    );
  }

  void _listener(BuildContext context, AddLocationState state) {
    if (state.onTap) {
      Navigator.pop(context, state.place);
    }
  }

  @override
  Widget build(BuildContext context) => BlocProvider<AddLocationBloc>(
    create: (_) => _addLocationBloc,
    child: BlocListener(
      bloc: _addLocationBloc,
      listener: _listener,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black87,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: TextField(
            controller: _searchQueryController,
            autofocus: true,
            decoration: InputDecoration(
              hintText: 'Search Location...',
              border: InputBorder.none,
              hintStyle: TextStyle(
                color: Colors.black38,
              ),
            ),
            style: TextStyle(
              color: Colors.black87,
            ),
            onChanged: (query) {
              _addLocationBloc.add(QueryChanged(query: query));
            },
          ), 
        ),
        body: BlocBuilder<AddLocationBloc, AddLocationState>(
          bloc: _addLocationBloc,
          builder: _searchSuggestions,
        ),
      ),
    ),
  );
}