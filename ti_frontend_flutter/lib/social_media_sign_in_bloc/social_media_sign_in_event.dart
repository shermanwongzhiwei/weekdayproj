import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class SocialMediaSignInEvent extends Equatable {
  final List<Object> props;

  SocialMediaSignInEvent([this.props = const []]);
}

// Bloc will debounce sign in after the first time
class SignInWithGooglePressed extends SocialMediaSignInEvent {}

class SignInWithFacebookPressed extends SocialMediaSignInEvent {}