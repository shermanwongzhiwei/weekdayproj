import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

@immutable
// Proper subset of what we need in EmailPasswordFormState
// May consider to merge with EmailPasswordFormState
class SocialMediaSignInState extends Equatable {
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isFirstTime;

  @override
  String toString() {
    return '''SocialMediaSignInState {
    isSubmitting: $isSubmitting,
    isSuccess: $isSuccess,
    isFailure $isFailure,
    isFirstTime $isFirstTime
    }''';
  }

  @override
  List<Object> get props =>
      [isSubmitting, isSuccess, isFailure, isFirstTime];

  SocialMediaSignInState({
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
    @required this.isFirstTime,
  }) {
    logger.d(this);
  }

  factory SocialMediaSignInState.empty() {
    return SocialMediaSignInState(
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isFirstTime: false,
    );
  }

  factory SocialMediaSignInState.loading() {
    return SocialMediaSignInState(
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
      isFirstTime: false,
    );
  }

  factory SocialMediaSignInState.failure() {
    return SocialMediaSignInState(
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      isFirstTime: false,
    );
  }

  factory SocialMediaSignInState.successFirstTime() {
    return SocialMediaSignInState(
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
      isFirstTime: true,
    );
  }

  factory SocialMediaSignInState.successNonFirstTime() {
    return SocialMediaSignInState(
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
      isFirstTime: false,
    );
  }
}
