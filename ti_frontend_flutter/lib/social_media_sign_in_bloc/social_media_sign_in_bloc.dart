import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:ti_frontend_flutter/social_media_sign_in_bloc/social_media_sign_in_event.dart';
import 'package:ti_frontend_flutter/social_media_sign_in_bloc/social_media_sign_in_state.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

class SocialMediaSignInBloc extends Bloc<SocialMediaSignInEvent, SocialMediaSignInState> {
  final UserRepository userRepository;

  SocialMediaSignInBloc({@required this.userRepository});

  @override
  SocialMediaSignInState get initialState => SocialMediaSignInState.empty();

  @override
  Stream<SocialMediaSignInState> mapEventToState(SocialMediaSignInEvent event) async* {
    logger.d('Social Media Sign In Event: ${event.runtimeType}');
    if (event is SignInWithGooglePressed) {
      yield* _mapSignInWithGooglePressedToState();
    } else if (event is SignInWithFacebookPressed) {
      yield* _mapSignInWithFacebookPressedToState();
    }
  }

  Stream<SocialMediaSignInState> _mapSignInWithGooglePressedToState() async* {
    yield SocialMediaSignInState.loading();
    try {
      await userRepository.signInWithGoogle();
      if (await userRepository.isFirstTime()) {
        yield SocialMediaSignInState.successFirstTime();
      } else {
        yield SocialMediaSignInState.successNonFirstTime();
      }
    } catch (e) {
      logger.e('Sign In With Google Pressed: $e');
      yield SocialMediaSignInState.failure();
    }
  }

  Stream<SocialMediaSignInState> _mapSignInWithFacebookPressedToState() async* {
    yield SocialMediaSignInState.loading();
    FacebookLoginResult result = await userRepository.signInWithFacebook();
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        await userRepository.firebaseAuthWithFacebook(token: result.accessToken);
        if (await userRepository.isFirstTime()) {
          yield SocialMediaSignInState.successFirstTime();
        } else {
          yield SocialMediaSignInState.successNonFirstTime();
        }
        break;
      case FacebookLoginStatus.cancelledByUser:
        logger.e('FacebookLoginStatus: Cancelled by user');
        yield SocialMediaSignInState.empty();
        break;
      case FacebookLoginStatus.error:
        logger.e('FacebookLoginStatus: ${result.errorMessage}');
        yield SocialMediaSignInState.failure();
        break;
    }
  }
}
