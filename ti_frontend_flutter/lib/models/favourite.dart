import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';

class Favourite extends Equatable {
  final String recommendationId;
  final bool hasSavedAsFavourite;
  Recommendation recommendation;

  Favourite({
    @required this.recommendationId,
    @required this.hasSavedAsFavourite,
    this.recommendation,
  }): super();

  @override
  List<Object> get props => [
    recommendationId,
    hasSavedAsFavourite,
    recommendation,
  ];

  @override
  String toString() => '''Favourite { 
    recommendationId: $recommendationId,
    hasSavedAsFavourite: $hasSavedAsFavourite,
    recommendation: $recommendation
    }''';
}