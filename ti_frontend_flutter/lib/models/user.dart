import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class User extends Equatable {
  final String email;
  final String username;
  final String name;
  final String profilePicture;
  final int followers;
  final int following;

  User({
    @required this.email,
    @required this.name, 
    @required this.username,
    @required this.profilePicture,
    @required this.followers,
    @required this.following,
  }): super();

  @override
  List<Object> get props => [
    email,
    name,
    username,
    profilePicture,
    followers,
    following,
  ];

  @override
  String toString() => '''User { 
    email: $email,
    name: $name,
    username: $username,
    profilePicture: $profilePicture,
    followers: $followers,
    following: $following
    }''';
}