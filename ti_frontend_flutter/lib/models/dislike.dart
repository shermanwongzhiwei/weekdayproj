import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';

class Dislike extends Equatable {
  final String recommendationId;
  final bool hasSavedAsDislike;
  Recommendation recommendation;

  Dislike({
    @required this.recommendationId,
    @required this.hasSavedAsDislike,
    this.recommendation,
  }): super();

  @override
  List<Object> get props => [
    recommendationId,
    hasSavedAsDislike,
    recommendation,
  ];

  @override
  String toString() => '''Dislike { 
    recommendationId: $recommendationId,
    hasSavedAsDislike: $hasSavedAsDislike,
    recommendation: $recommendation
    }''';
}