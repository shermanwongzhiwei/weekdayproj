import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:location/location.dart';

@immutable
abstract class LocationResult extends Equatable {
  final LocationData location;

  List<Object> get props => [location];

  LocationResult(this.location);
}

class CurrentLocation extends LocationResult {
  CurrentLocation(LocationData location): super(location);
}

class LastKnownLocation extends LocationResult {
  LastKnownLocation(LocationData location): super(location);
}

class NoLocation extends LocationResult {
  static final _location = LocationData.fromMap({
    'latitude': 3.065,
    'longitude': 101.601,
  });

  NoLocation(): super(_location);
}