import 'package:equatable/equatable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Recommendation extends Equatable {
  final String id;
  final String description;
  final String dish;
  final String googleLink;
  final List<String> pictures;
  final String restaurant;
  final DateTime timestamps;
  final GeoPoint location;
  final String name;
  final String username;
  final double distance;
  final int favourites;

  Recommendation({
    this.id, 
    this.dish,
    this.restaurant,
    this.googleLink,
    this.description,
    this.pictures,
    this.name,
    this.username,
    this.timestamps,
    this.location,
    this.distance,
    this.favourites,
  }): super();

  @override
  List<Object> get props => [
    id,
    dish,
    restaurant,
    googleLink,
    description,
    pictures,
    name,
    timestamps,
    location,
    distance,
  ];

  @override
  String toString() => '''Recommendation { 
    id: $id,
    dish: $dish,
    restaurant: $restaurant,
    googleLink: $googleLink,
    description: $description,
    imageLink: $pictures,
    name: $name,
    username: $username,
    timestamps: $timestamps,
    location: $location,
    distance: $distance
    }''';
}