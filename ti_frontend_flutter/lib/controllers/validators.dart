import 'package:form_field_validator/form_field_validator.dart';


class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  // Minimum eight characters, at least one letter and one number
  static final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
  );

  static String emailErrorMessage(String email) {
    String value;
    if (email.isEmpty) {
      value = 'Email cannot be left blank';
    } else if (!_emailRegExp.hasMatch(email)) {
      value = 'Invalid email';
    }
    return value;
  }

  static String passwordErrorMessage(String password) {
    String value;
    if (password.isEmpty) {
      value = 'Password cannot be left blank';
    } else if (password.length <= 8) {
      value = 'Must be longer than 8 characters';
    }
    else if (!_passwordRegExp.hasMatch(password)) {
      value = 'Must have at least one letter and one number';
    }
    return value;
  }

  static EmailValidator getEmailValidator() {
    return EmailValidator(
      errorText: 'Invalid email',
    );
  }

  static MultiValidator getPasswordValidator() {
    return MultiValidator([  
      RequiredValidator(errorText: 'Password is required'),  
      MinLengthValidator(8, errorText: 'Password must be at least 8 characters long'),  
      PatternValidator(r'(?=.*?[#?!@$%^&*-])', errorText: 'Passwords must have at least one special character')  
    ]);
  }
}
