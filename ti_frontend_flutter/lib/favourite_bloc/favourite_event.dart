import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class FavouriteEvent extends Equatable {
  final List<Object> props;

  FavouriteEvent([this.props = const []]);
}

class Fetch extends FavouriteEvent {}

class Refresh extends FavouriteEvent {}