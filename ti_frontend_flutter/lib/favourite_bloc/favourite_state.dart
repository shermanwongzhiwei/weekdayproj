import 'package:equatable/equatable.dart';
import 'package:ti_frontend_flutter/models/favourite.dart';

abstract class FavouriteState extends Equatable {
  final List<Object> props;

  FavouriteState([this.props = const []]) : super();
}

class FavouriteUninitialized extends FavouriteState {}

class FavouriteError extends FavouriteState {}

class FavouriteLoaded extends FavouriteState {
  final List<Favourite> favourites;
  final bool hasReachedMax;

  FavouriteLoaded({
    this.favourites,
    this.hasReachedMax,
  }) : super([favourites, hasReachedMax]);

  FavouriteLoaded copyWith({
    List<Favourite> favourites,
    bool hasReachedMax,
  }) {
    return FavouriteLoaded(
      favourites: favourites ?? this.favourites,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }
}