import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:ti_frontend_flutter/favourite_bloc/bloc.dart';
import 'package:ti_frontend_flutter/services/favourite_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

class FavouriteBloc extends Bloc<FavouriteEvent, FavouriteState> {
  final FavouriteRepository _favouriteRepository;
  
  FavouriteBloc({FavouriteRepository favouriteRepository})
    : _favouriteRepository = favouriteRepository;

  bool _hasReachedMax(FavouriteState state) => state is FavouriteLoaded && state.hasReachedMax;
  
  @override
  FavouriteState get initialState => FavouriteUninitialized();

  @override
  Stream<FavouriteState> transformEvents(
    Stream<FavouriteEvent> events,
    Stream<FavouriteState> Function(FavouriteEvent event) next,
  ) {
    return super.transformEvents(
      events.debounce((_) => TimerStream(true, Duration(milliseconds: 500))),
      next,
    );
  }

  @override
  Stream<FavouriteState> mapEventToState(FavouriteEvent event) async* {
    if (event is Fetch && !_hasReachedMax(state)) {
      try {
        if (state is FavouriteUninitialized) {
          final favourites = await _favouriteRepository.fetchFavourites(limit: 5);
          yield FavouriteLoaded(favourites: favourites, hasReachedMax: false);
        }
        if (state is FavouriteLoaded) {
          final favourites = await _favouriteRepository.fetchFavourites(limit: 5);
          yield favourites.isEmpty
            ? (state as FavouriteLoaded).copyWith(hasReachedMax: true)
            : FavouriteLoaded(
                favourites: (state as FavouriteLoaded).favourites + favourites, hasReachedMax: false);
        }
      } catch (e) {
        logger.e('FavouriteError: $e');
        yield FavouriteError();
      }
    } else if (event is Refresh){
      try {
        _favouriteRepository.refreshFavourite();
        final favourites = await _favouriteRepository.fetchFavourites(limit: 5);
        yield FavouriteLoaded(favourites: favourites, hasReachedMax: false);
      } catch (e) {
        logger.e('FavouriteError: $e');
        yield FavouriteError();
      }
    }
  }
}