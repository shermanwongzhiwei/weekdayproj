import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';
import 'package:ti_frontend_flutter/util/ui_helpers.dart';
import 'package:recase/recase.dart';

class RecommendationCard extends StatelessWidget {
  final Recommendation recommendation;

  const RecommendationCard({Key key, @required this.recommendation}) : super(key: key);

  Widget thumbnail({BuildContext context}) {
    if (recommendation.pictures.length > 0) {
      return Stack(
        children: <Widget>[
          Center(child: CircularProgressIndicator()),
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Center(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: recommendation.pictures[0],
              ),
            ),
          ),
        ],
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          decoration: BoxDecoration(color: Color(0xff439C68)),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: SizedBox(
        height: 90,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.33,
              child: thumbnail(context: context),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15.0, 0.0, 2.0, 0.0),
                child: _CardDescription(
                  dish: recommendation.dish,
                  description: recommendation.description,
                  restaurant: recommendation.restaurant,
                  distance: recommendation.distance.toStringAsFixed(1),
                  name: recommendation.name,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _CardDescription extends StatelessWidget {
  final String dish;
  final String description;
  final String restaurant;
  final String distance;
  final String name;

  _CardDescription({
    Key key,
    this.dish,
    this.description,
    this.restaurant,
    this.distance,
    this.name,
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    ReCase rc = new ReCase(dish);
    String dishTitled = rc.titleCase;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 5,
              child:            
                // padding: EdgeInsets.only(top: 10.0, bottom: 6.0, right: 5.0),
                Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$dishTitled', 
                      maxLines: 1,
                      style: const TextStyle(
                        color: Colors.black,
                        fontFamily: 'Montserrat',
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      '@ $restaurant',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontFamily: 'OpenSans',
                        fontSize: 12.0,
                        color: const Color(0xFF757575)
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 6.0),
                      child: ('$description' != 'null' ? Text(
                        '$description',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontFamily: 'OpenSans',
                          fontSize: 10.0,
                          color: Colors.black,
                        ),
                      ) : UIHelpers.mBox),
                    ),
                  ],
                ),
              
        ),
        Expanded(
          flex: 1,
          child: Row(
            // crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '$distance km away',//★',
                style: const TextStyle(
                  fontSize: 11.0,
                  fontFamily: 'OpenSans',
                  fontWeight: FontWeight.bold,
                  color: Color(0xffff5b5a),
                ),
              ),
              Text(
                '$name',
                style: const TextStyle(
                  fontSize: 9.0,
                  fontFamily: 'Montserrat',
                  color: Colors.black,
                ),
              ),              
            ],
          ),
        ),
      ],
    );
  }
}
