//import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:ti_frontend_flutter/blocs/sign_in_bloc.dart';
//import 'package:ti_frontend_flutter/blocs/sign_in_event.dart';
//import 'package:ti_frontend_flutter/blocs/sign_in_state.dart';
//import 'package:ti_frontend_flutter/screens/main_bottom_navigation.dart';
//
//class LoginFormCard extends StatefulWidget {
//  LoginFormCard({Key key}) : super(key: key);
//
//  State<LoginFormCard> createState() => _LoginFormCardState();
//}
//
//class _LoginFormCardState extends State<LoginFormCard> {
//  final TextEditingController _emailController = TextEditingController();
//  final TextEditingController _passwordController = TextEditingController();
//
//  SignInBloc _loginBloc;
//
//  @override
//  void initState() {
//    super.initState();
//    _loginBloc = BlocProvider.of<SignInBloc>(context);
//  }
//
//  BoxDecoration _cardBox() => BoxDecoration(
//        color: Colors.white,
//        borderRadius: BorderRadius.circular(8),
//        boxShadow: [
//          BoxShadow(color: Colors.black12, offset: Offset(0, 15), blurRadius: 15),
//          BoxShadow(color: Colors.black12, offset: Offset(0, -10), blurRadius: 10),
//        ],
//      );
//
//  Widget _forgotPassword() => Row(
//        mainAxisAlignment: MainAxisAlignment.end,
//        children: <Widget>[
//          Text('Forgot Password?', style: TextStyle(color: Colors.blue, fontSize: 15))
//        ],
//      );
//
//  BoxDecoration _loginButtonDecoration() => BoxDecoration(
//          gradient: LinearGradient(colors: [Colors.red, Colors.deepOrange]),
//          borderRadius: BorderRadius.circular(6),
//          boxShadow: [
//            BoxShadow(color: Colors.black12.withOpacity(0.3), offset: Offset(0, 8), blurRadius: 8),
//          ]);
//
//  Widget _loginButton() => Row(
//        mainAxisAlignment: MainAxisAlignment.end,
//        children: <Widget>[
//          Container(
//            width: 140,
//            height: 45,
//            decoration: _loginButtonDecoration(),
//            child: Material(
//              color: Colors.transparent,
//              child: InkWell(
//                onTap: _onLoginButtonPressed,
//                child: Center(
//                    child: Text('Log In', style: TextStyle(color: Colors.white, fontSize: 18))),
//              ),
//            ),
//          ),
//        ],
//      );
//
//  void _onLoginButtonPressed() {
//    _loginBloc
//        .add(SignInSubmitted(email: _emailController.text, password: _passwordController.text));
//  }
//
//  void _blocListener(BuildContext context, SignInState state) {
//    if (state is SignInSuccessState) {
//      Navigator.pushReplacement(
//          context, MaterialPageRoute(builder: (context) => MainBottomNavigation()));
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) => BlocListener(
//        bloc: _loginBloc,
//        listener: _blocListener,
//        child: Container(
//          width: double.infinity,
//          height: 250,
//          decoration: _cardBox(),
//          child: Padding(
//            padding: EdgeInsets.all(20),
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: <Widget>[
//                // Text('User email', style: TextStyle(fontSize: 15)),
//                TextField(
//                  controller: _emailController,
//                  decoration: InputDecoration(
//                    prefixIcon: Icon(Icons.email),
//                    hintText: "Email",
//                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12),
//                  ),
//                ),
//                Spacer(flex: 1),
//                // Text('Password', style: TextStyle(fontSize: 15)),
//                TextField(
//                  controller: _passwordController,
//                  obscureText: true,
//                  decoration: InputDecoration(
//                    prefixIcon: Icon(Icons.lock),
//                    hintText: "Password",
//                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12),
//                  ),
//                ),
//                Spacer(flex: 1),
//                _forgotPassword(),
//                Spacer(flex: 1),
//                _loginButton(),
//              ],
//            ),
//          ),
//        ),
//      );
//}
