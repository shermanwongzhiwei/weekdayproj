import 'package:flutter/material.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:ti_frontend_flutter/models/favourite.dart' as Favourite;
import 'package:recase/recase.dart';

class FavouriteCard extends StatelessWidget {
  final Favourite.Favourite favourite;

  const FavouriteCard({Key key, @required this.favourite}) : super(key: key);

  Recommendation get recommendation => favourite.recommendation;

  Widget thumbnail({BuildContext context}) {
    if (recommendation.pictures.length > 0) {
      return Stack(
        children: <Widget>[
          Center(child: CircularProgressIndicator()),
          Center(
            child: FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: recommendation.pictures[0],
            ),
          ),
        ],
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          decoration: BoxDecoration(color: Color(0xff439C68)),
        ),
      );
      
      // Container(
      //   decoration: BoxDecoration(color: Theme.of(context).primaryColor),
      // );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: SizedBox(
        height: 90,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.33,
              child: thumbnail(context: context),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15.0, 0.0, 2.0, 0.0),
                child: _FavouriteDescription(
                  dish: recommendation.dish,
                  restaurant: recommendation.restaurant,
                  distance: recommendation.distance,
                  favourites: recommendation.favourites ?? 0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _FavouriteDescription extends StatelessWidget {
  final String dish;
  final String restaurant;
  final double distance;
  final int favourites;

  const _FavouriteDescription({
    Key key,
    this.dish,
    this.restaurant,
    this.distance,
    this.favourites,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ReCase rc = new ReCase(dish);
    String dishTitled = rc.titleCase;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            '$dishTitled',
            maxLines: 1,
            style: const TextStyle(
              color: Colors.black,
              fontFamily: 'Montserrat',
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            '@ $restaurant',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 12.0,
              color: const Color(0xFF757575)
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon(
                    Icons.thumb_up,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                Text(
                  '$favourites favourite(s)',
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            )
          ),
        ],
      ),
    );
  }
}