import 'package:flutter/material.dart';
import 'package:ti_frontend_flutter/util/logging.dart';


class CustomNavigatorObserver extends NavigatorObserver {
  List<Route<dynamic>> routeStack = List();

  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) {
    routeStack.add(route);
    logger.v('Route stack: $routeStack');
  }

  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {
    routeStack.removeLast();
    logger.v('Route stack: $routeStack');
  }

  @override
  void didRemove(Route route, Route previousRoute) {
    routeStack.removeLast();
    logger.v('Route stack: $routeStack');
  }

  @override
  void didReplace({Route newRoute, Route oldRoute}) {
    routeStack.removeLast();
    routeStack.add(newRoute);
    logger.v('Route stack: $routeStack');
  }
}