import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ti_frontend_flutter/models/location_result.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';

abstract class HomeFeedState extends Equatable {
  final List<Object> props;

  HomeFeedState([this.props = const []]) : super();
}

class HomeFeedUninitialized extends HomeFeedState {}

class HomeFeedError extends HomeFeedState {}

class HomeFeedLoaded extends HomeFeedState {
  final List<Recommendation> recommendations;
  final bool hasReachedMax;
  final LocationResult locationResult;

  HomeFeedLoaded({
    @required this.recommendations,
    @required this.hasReachedMax,
    @required this.locationResult,
  }) : super([recommendations, hasReachedMax, locationResult]);

  HomeFeedLoaded copyWith({
    List<Recommendation> recommendations,
    bool hasReachedMax,
    LocationResult locationResult,
  }) {
    return HomeFeedLoaded(
      recommendations: recommendations ?? this.recommendations,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      locationResult: locationResult ?? this.locationResult,
    );
  }
}