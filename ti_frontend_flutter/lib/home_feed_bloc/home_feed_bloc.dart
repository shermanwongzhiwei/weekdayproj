import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:ti_frontend_flutter/home_feed_bloc/bloc.dart';
import 'package:ti_frontend_flutter/models/location_result.dart';
import 'package:ti_frontend_flutter/services/recommendation_repository.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

class HomeFeedBloc extends Bloc<HomeFeedEvent, HomeFeedState> {
  final RecommendationRepository _recommendationRepository;
  
  HomeFeedBloc({RecommendationRepository recommendationRepository})
    : _recommendationRepository = recommendationRepository;

  bool _hasReachedMax(HomeFeedState state) => state is HomeFeedLoaded && state.hasReachedMax;
  
  @override
  HomeFeedState get initialState => HomeFeedUninitialized();

  @override
  Stream<HomeFeedState> transformEvents(
    Stream<HomeFeedEvent> events,
    Stream<HomeFeedState> Function(HomeFeedEvent event) next,
  ) {
    return super.transformEvents(
      events.debounce((_) => TimerStream(true, Duration(milliseconds: 500))),
      next,
    );
  }

  @override
  Stream<HomeFeedState> mapEventToState(HomeFeedEvent event) async* {
    final LocationResult locationResult = await _recommendationRepository.getUserLocation();
    if (event is Fetch && !_hasReachedMax(state)) {
      try {
        if (state is HomeFeedUninitialized) {
          final recommendations = await _recommendationRepository.fetchRecommendations(5, locationResult.location);
          yield HomeFeedLoaded(recommendations: recommendations, hasReachedMax: false, locationResult: locationResult);
        } else if (state is HomeFeedLoaded) {
          final recommendations = await _recommendationRepository.fetchRecommendations(5, locationResult.location);
          yield recommendations.isEmpty
            ? (state as HomeFeedLoaded).copyWith(hasReachedMax: true)
            : HomeFeedLoaded(
                recommendations: (state as HomeFeedLoaded).recommendations + recommendations,
                hasReachedMax: false,
                locationResult: locationResult,
              );
        }
      } catch (e) {
        logger.e('HomeFeedError: $e');
        yield HomeFeedError();
      }
    } else if (event is Refresh){
      try{
        _recommendationRepository.refreshRecommendations();
        final recommendations = await _recommendationRepository.fetchRecommendations(5, locationResult.location);
        yield HomeFeedLoaded(recommendations: recommendations, hasReachedMax: false, locationResult: locationResult);
      } catch (e) {
        logger.e('HomeFeedError: $e');
        yield HomeFeedError();
      }
    }
  }
}