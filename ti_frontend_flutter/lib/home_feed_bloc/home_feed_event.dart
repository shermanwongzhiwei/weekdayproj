import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class HomeFeedEvent extends Equatable {
  final List<Object> props;

  HomeFeedEvent([this.props = const []]);
}

class Fetch extends HomeFeedEvent {}

class Refresh extends HomeFeedEvent {}