import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:ti_frontend_flutter/models/user.dart';


class UserRepository {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;
  final FacebookLogin _facebookLogin;
  final Firestore _firestore;

  UserRepository({FirebaseAuth auth, GoogleSignIn googleSignIn, FacebookLogin facebookLogin, Firestore firestore})
    : _firebaseAuth = auth ?? FirebaseAuth.instance,
      _googleSignIn = googleSignIn ?? GoogleSignIn(),
      _facebookLogin = facebookLogin ?? FacebookLogin(),
      _firestore = firestore ?? Firestore.instance;

  Future<void> signUp({String email, String password}) async {
    return await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> signInWithEmailAndPassword({String email, String password}) async {
    return await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<FirebaseUser> signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    await _firebaseAuth.signInWithCredential(credential);
    return _firebaseAuth.currentUser();
  }

  Future<FacebookLoginResult> signInWithFacebook() async {
    final result = await _facebookLogin.logIn(['email']);
    return result;
  }

  Future<FirebaseUser> firebaseAuthWithFacebook({FacebookAccessToken token}) async {
    AuthCredential credential= FacebookAuthProvider.getCredential(accessToken: token.token);
    await _firebaseAuth.signInWithCredential(credential);
    return _firebaseAuth.currentUser();
  }

  Future<bool> isSignedIn() async {
    final currentUser = await _firebaseAuth.currentUser();
    return currentUser != null;
  }

  Future<bool> isFirstTime() async {
    final currentUser = await _firebaseAuth.currentUser();
    final userData = await _firestore
      .collection('users')
      .document(currentUser.email)
      .get();
    return !userData.exists;
  }

  Future<User> initializeUser(String name) async {
    final currentUser = await _firebaseAuth.currentUser();
    await _firestore
      .collection('users')
      .document(currentUser.email)
      .setData({
        'username': currentUser.email,
        'name': name,
        'followers': 0,
        'following': 0,
      });
    return await getUser();
  }

  Future<void> signOut() async {
    return Future.wait([
      _firebaseAuth.signOut(),
      _googleSignIn.signOut(),
      _facebookLogin.logOut(),
    ]);
  }

  Future<User> getUser() async {
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    final userData = await _firestore
      .collection('users')
      .document(currentUser.email)
      .get();

    return User(
      email: currentUser.email,
      name: userData.data['name'],
      username: userData.data['username'],
      profilePicture: userData.data['profilePicture'],
      followers: userData.data['followers'],
      following: userData.data['following'],
    );
  }
}
