import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ti_frontend_flutter/models/dislike.dart';
import 'package:ti_frontend_flutter/services/recommendation_repository.dart';

class DislikeRepository {
  final Firestore _firestore;
  final FirebaseAuth _firebaseAuth;

  DislikeRepository({Firestore firestore, FirebaseAuth firebaseAuth, RecommendationRepository recommendationRepository})
    : _firestore = firestore ?? Firestore.instance,
      _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  Future<void> saveAsDislike({
    String recommendationId,
  }) async {
    String email = await getUserEmail();

    await _firestore
      .collection('dislikes')
      .add({
        'recommendationId': recommendationId,
        'userEmail': email,
        'timestamps': FieldValue.serverTimestamp(),
      });

    DocumentReference ref = _firestore
      .collection('recommendations')
      .document(recommendationId);

    DocumentSnapshot snapshot = await ref.get();

    if (snapshot.exists && snapshot['dislikes'] != null) {
      _firestore.runTransaction((Transaction tx) async {
        DocumentSnapshot snapshot = await tx.get(ref);
        if (snapshot.exists) {
          await tx.update(ref, <String, int>{
            'dislikes': snapshot.data['dislikes'] + 1,
          });
        }
      });
    } else {
      ref.updateData({
        'dislikes': 1,
      });
    }
  }

  Future<void> removeFromDislike({
    String recommendationId,
  }) async {
    String email = await getUserEmail();

    await _firestore
      .collection('dislikes')
      .where(
        'recommendationId',
        isEqualTo: recommendationId,
      ).where(
        'userEmail',
        isEqualTo: email,
      ).getDocuments().then((snapshot) {
        for (DocumentSnapshot ds in snapshot.documents){
          ds.reference.delete();
        }
      });

    DocumentReference ref = _firestore
      .collection('recommendations')
      .document(recommendationId);

    DocumentSnapshot snapshot = await ref.get();

    if (snapshot.exists && snapshot['dislikes'] != null && snapshot['dislikes'] > 0) {
      _firestore.runTransaction((Transaction tx) async {
        DocumentSnapshot snapshot = await tx.get(ref);
        if (snapshot.exists) {
          await tx.update(ref, <String, int>{
            'dislikes': snapshot.data['dislikes'] - 1,
          });
        }
      });
    }
  }

  Future<Dislike> getDislikeStatus({
    String recommendationId,
  }) async {
    String email = await getUserEmail();

    QuerySnapshot snapshot = await _firestore
      .collection('dislikes')
      .where(
        'recommendationId',
        isEqualTo: recommendationId,
      ).where(
        'userEmail',
        isEqualTo: email,
      ).getDocuments();

    print(snapshot.documents.length > 0);

    return Dislike(
      recommendationId: recommendationId, 
      hasSavedAsDislike: snapshot.documents.length > 0,
    );
  }

  Future<String> getUserEmail() async {
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    return currentUser.email;
  }
}