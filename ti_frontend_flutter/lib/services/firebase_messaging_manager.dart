import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

class FirebaseMessagingManager extends StatefulWidget {
  final Widget child;

  @override
  _FirebaseMessagingManagerState createState() => _FirebaseMessagingManagerState();

  FirebaseMessagingManager({@required this.child});
}

class _FirebaseMessagingManagerState extends State<FirebaseMessagingManager> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    fcmListeners();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
    );
  }

  void fcmListeners() async {
    if (Platform.isIOS) iOSPermission();

    await _firebaseMessaging.getToken().then((token) {
      logger.d('Firebase messaging token: $token');
    }).timeout(Duration(seconds: 2), onTimeout: () {
      logger.d('timeout');
    });
    bool isConfigured = false;
    if (!isConfigured) {
      logger.i("Initial configure of FCM");
      // TODO: Check firebase repo issue, everytime a message is received, some how each callback is called twice, temporary solution is to use stopwatch to intercept the second message
      final stopwatch = Stopwatch();
      isConfigured = true;
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          processMessage(message, Type.onMessage, stopwatch);
        },
        onResume: (Map<String, dynamic> message) async {
          processMessage(message, Type.onResume, stopwatch);
        },
        onLaunch: (Map<String, dynamic> message) async {
          processMessage(message, Type.onLaunch, stopwatch);
        },
      );
    }
  }

  void processMessage(message, Type type, Stopwatch stopwatch) async {
    // Object returned from firebase has a different type, so need encode and decode again
    message = json.decode(json.encode(message));
    logger.d(message);
//    FirebaseNotification firebaseNotification = FirebaseNotification.fromJson(message);

    if (stopwatch.isRunning) {
      logger.d('Stopwatch stop');
      stopwatch.stop();
      return;
    }
    stopwatch.start();

    switch (type) {
      case Type.onMessage:
      case Type.onResume:
      case Type.onLaunch:
        Fluttertoast.showToast(msg: 'Processed firebase cloud notification');
    }
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {});
  }
}

enum Type { onMessage, onResume, onLaunch }
