import 'dart:math';
import 'dart:typed_data';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:location/location.dart' as LocationService;

import 'package:ti_frontend_flutter/models/location_result.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';

class RecommendationRepository {
  final Firestore _firestore;
  final FirebaseStorage _firebaseStorage;
  final FirebaseAuth _firebaseAuth;
  final LocationService.Location _location;
  final GoogleMapsPlaces _googleMapsPlaces;
  DocumentSnapshot _lastVisible;

  RecommendationRepository({
    LocationService.Location location, 
    GoogleMapsPlaces googleMapsPlaces, 
    Firestore firestore, 
    FirebaseStorage firebaseStorage, 
    FirebaseAuth firebaseAuth
  })
    : _firestore = firestore ?? Firestore.instance,
      _firebaseStorage = firebaseStorage ?? FirebaseStorage.instance,
      _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
      _location = location ?? LocationService.Location(),
      _googleMapsPlaces = googleMapsPlaces ?? GoogleMapsPlaces(apiKey: 'AIzaSyAATfNUnK6aLJK_I67NwwoiCdBwrdFFDmQ');

  Future<List<Recommendation>> fetchRecommendations(int limit, LocationService.LocationData locationData) async {
    Query query = _firestore
      .collection('recommendations')
      .orderBy('timestamps');

    if (_lastVisible != null) {
      query = query.startAfterDocument(_lastVisible);
    }

    QuerySnapshot data = await query.getDocuments();

    if (data != null && data.documents.length > 0) {
      _lastVisible = data.documents[data.documents.length - 1];
    }

    List<Recommendation> recommendations = data
      .documents
      .map(parseRecommendation(locationData))
      .toList();

    recommendations.sort((a, b) => a.distance.compareTo(b.distance));
    return recommendations;
  }

  Future<Recommendation> fetchRecommendation({
    @required String recommendationId, 
    @required LocationService.LocationData locationData
  }) async {
    DocumentSnapshot rawRecommendation = await _firestore
      .collection('recommendations')
      .document(recommendationId)
      .get();

    return parseRecommendation(locationData)(rawRecommendation);
  }

  Recommendation Function(DocumentSnapshot) parseRecommendation(LocationService.LocationData locationData) {
    return (DocumentSnapshot rawRecommendation) => Recommendation(
      id: rawRecommendation.documentID,
      dish: rawRecommendation['dish'],
      restaurant: rawRecommendation['restaurant'],
      googleLink: rawRecommendation['googleLink'],
      pictures: rawRecommendation['pictures'] != null ? rawRecommendation['pictures'].cast<String>() : [],
      description: rawRecommendation['description'],
      name: rawRecommendation['name'],
      username: rawRecommendation['username'],
      timestamps: rawRecommendation['timestamps'].toDate(),
      location: rawRecommendation['location'] as GeoPoint,
      distance: calculateDistance(
        (rawRecommendation['location'] as GeoPoint).latitude,
        (rawRecommendation['location'] as GeoPoint).longitude,
        locationData.latitude,
        locationData.longitude,
      ),
      favourites: rawRecommendation['favourites'],
    );
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;
    return 12742 * asin(sqrt(a));
  }

  Future<Recommendation> postRecommendation({
    String description,
    String dish,
    PlacesSearchResult restaurant,
    List<Asset> images
  }) async {
    PlacesDetailsResponse response = await _googleMapsPlaces.getDetailsByPlaceId(restaurant.placeId);
    PlaceDetails details = response.result;

    DocumentReference documentReference = await _firestore.collection('recommendations')
      .add({
        'description': description,
        'dish': dish,
        'restaurant': restaurant.name,
        'location': GeoPoint(
          restaurant.geometry.location.lat,
          restaurant.geometry.location.lng,
        ),
        'googleLink': details.url,
        'timestamps': FieldValue.serverTimestamp(),
        'name': await getUserName(),
        'username': (await _firebaseAuth.currentUser()).email,
        'favourites': 0,
      });

    print('Document added: ${documentReference.documentID}');
    print((await _firestore.collection('recommendations').document(documentReference.documentID).get()).exists);

    for (var i = 0; i < images.length; i++) {
      await uploadImage(documentReference.documentID, images[i], i);
    }

    LocationResult locationResult = await getUserLocation();
    return await fetchRecommendation(recommendationId: documentReference.documentID, locationData: locationResult.location);
  }

  Future<void> uploadImage(String id, Asset image, int index) async {
    ByteData byteData = await image.getByteData();
    List<int> imageData = byteData.buffer.asUint8List();

    StorageReference storageReference = _firebaseStorage
      .ref()    
      .child('recommendations/$id/picture$index');

    StorageUploadTask uploadTask = storageReference.putData(imageData);

    await uploadTask.onComplete;    
    print('File Uploaded');    

    storageReference.getDownloadURL().then((fileURL) {    
      _firestore.collection('recommendations').document(id).updateData({
        'pictures': FieldValue.arrayUnion([fileURL]),
      });
    });
  }

  void refreshRecommendations() {
    _lastVisible = null;
  }

  Future<String> getUserName() async {
    final currentUser = await _firebaseAuth.currentUser();
    final userData = await _firestore
      .collection('users')
      .document(currentUser.email)
      .get();
    return userData.data['name'];
  }

  Future<LocationResult> getUserLocation() async {
    final currentUser = await _firebaseAuth.currentUser();
    final userDoc = _firestore
      .collection('users')
      .document(currentUser.email);

    if (await _location.serviceEnabled()) {
      LocationService.LocationData locationData = await _location.getLocation();
      userDoc.updateData({
        'lastLocation': GeoPoint(locationData.latitude, locationData.longitude),
      });
      return CurrentLocation(locationData);
    } else {
      final userData = await userDoc.get();
      GeoPoint location = userData.data['lastLocation'] as GeoPoint;

      if (location != null) {
        return LastKnownLocation(
          LocationService.LocationData.fromMap({
            'latitude': location.latitude,
            'longitude': location.longitude
          }),
        );
      } else {
        return NoLocation();
      }
    }
  }
}