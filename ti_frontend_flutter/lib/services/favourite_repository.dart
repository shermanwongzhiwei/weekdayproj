import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ti_frontend_flutter/models/favourite.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';
import 'package:ti_frontend_flutter/models/location_result.dart';
import 'package:ti_frontend_flutter/services/recommendation_repository.dart';

class FavouriteRepository {
  final Firestore _firestore;
  final FirebaseAuth _firebaseAuth;
  final RecommendationRepository _recommendationRepository;
  DocumentSnapshot _lastVisible;

  FavouriteRepository({Firestore firestore, FirebaseAuth firebaseAuth, RecommendationRepository recommendationRepository})
    : _firestore = firestore ?? Firestore.instance,
      _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
      _recommendationRepository = recommendationRepository ?? RecommendationRepository();

  Future<List<Favourite>> fetchFavourites({int limit}) async {
    String email = await getUserEmail();

    Query query = _firestore
      .collection('favourites')
      .where(
        'userEmail',
        isEqualTo: email,
      ).orderBy(
        'timestamps',
        descending: true,
      );

    if (_lastVisible != null) {
      query = query.startAfterDocument(_lastVisible);
    }
    query = query.limit(limit);

    QuerySnapshot data = await query.getDocuments();

    if (data != null && data.documents.length > 0) {
      _lastVisible = data.documents[data.documents.length - 1];
    }

    List<Favourite> favourites = data.documents.map((rawFavourite) {
      return Favourite(
        recommendationId: rawFavourite['recommendationId'],
        hasSavedAsFavourite: true,
      );
    }).toList();

    LocationResult locationResult = await _recommendationRepository.getUserLocation();
    List<Recommendation> recommendations = await Future.wait(favourites.map((favourite) {
      return _recommendationRepository.fetchRecommendation(
        recommendationId: favourite.recommendationId, 
        locationData: locationResult.location
      );
    }));

    for (var i = 0; i < favourites.length; i++) {
      favourites[i].recommendation = recommendations[i];
    }

    return favourites;
  }

  Future<void> saveAsFavourite({
    String recommendationId,
  }) async {
    String email = await getUserEmail();

    await _firestore
      .collection('favourites')
      .add({
        'recommendationId': recommendationId,
        'userEmail': email,
        'timestamps': FieldValue.serverTimestamp(),
      });

    DocumentReference ref = _firestore
      .collection('recommendations')
      .document(recommendationId);

    DocumentSnapshot snapshot = await ref.get();

    if (snapshot.exists && snapshot['favourites'] != null) {
      _firestore.runTransaction((Transaction tx) async {
        DocumentSnapshot snapshot = await tx.get(ref);
        if (snapshot.exists) {
          await tx.update(ref, <String, int>{
            'favourites': snapshot.data['favourites'] + 1,
          });
        }
      });
    } else {
      ref.updateData({
        'favourites': 1,
      });
    }
  }

  Future<void> removeFromFavourite({
    String recommendationId,
  }) async {
    String email = await getUserEmail();

    await _firestore
      .collection('favourites')
      .where(
        'recommendationId',
        isEqualTo: recommendationId,
      ).where(
        'userEmail',
        isEqualTo: email,
      ).getDocuments().then((snapshot) {
        for (DocumentSnapshot ds in snapshot.documents){
          ds.reference.delete();
        }
      });

    DocumentReference ref = _firestore
      .collection('recommendations')
      .document(recommendationId);

    DocumentSnapshot snapshot = await ref.get();

    if (snapshot.exists && snapshot['favourites'] != null && snapshot['favourites'] > 0) {
      _firestore.runTransaction((Transaction tx) async {
        DocumentSnapshot snapshot = await tx.get(ref);
        if (snapshot.exists) {
          await tx.update(ref, <String, int>{
            'favourites': snapshot.data['favourites'] - 1,
          });
        }
      });
    }
  }

  Future<Favourite> getFavouriteStatus({
    String recommendationId,
  }) async {
    String email = await getUserEmail();

    QuerySnapshot snapshot = await _firestore
      .collection('favourites')
      .where(
        'recommendationId',
        isEqualTo: recommendationId,
      ).where(
        'userEmail',
        isEqualTo: email,
      ).getDocuments();

    print(snapshot.documents.length > 0);

    return Favourite(
      recommendationId: recommendationId, 
      hasSavedAsFavourite: snapshot.documents.length > 0,
    );
  }

  void refreshFavourite() {
    _lastVisible = null;
  }

  Future<String> getUserEmail() async {
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    return currentUser.email;
  }
}