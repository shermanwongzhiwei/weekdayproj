import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ti_frontend_flutter/models/user.dart';
import 'package:ti_frontend_flutter/util/logging.dart';

@immutable
abstract class AuthenticationState extends Equatable {
  final List<Object> props;

  AuthenticationState([this.props = const []]) : super() {
    logger.v(this);
  }
}

class Uninitialized extends AuthenticationState {}

class FirstTimeAuthenticated extends AuthenticationState {}

class NonFirstTimeAuthenticated extends AuthenticationState {
  final User user;

  NonFirstTimeAuthenticated(this.user) : super([user]);
}

class Unauthenticated extends AuthenticationState {}