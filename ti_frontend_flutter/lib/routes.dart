import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:ti_frontend_flutter/models/recommendation.dart';
import 'package:ti_frontend_flutter/models/user.dart';
import 'package:ti_frontend_flutter/screens/main_bottom_navigation.dart';
import 'package:ti_frontend_flutter/screens/recommendation_details.dart';
import 'package:ti_frontend_flutter/screens/sign_in_email.dart';
import 'package:ti_frontend_flutter/screens/sign_up_email.dart';
import 'package:ti_frontend_flutter/screens/sign_in.dart';
import 'package:ti_frontend_flutter/screens/add_location.dart';
import 'package:ti_frontend_flutter/screens/provide_username.dart';
import 'package:ti_frontend_flutter/services/user_repository.dart';

const signin = '/';
const sign_up_email = '/sign_up_email';
const sign_in_email = '/sign_in_email';
const provide_username = '/provide_username';
const home = '/home';
const recommendation_details = '/recommendation_details';
const add_location = '/add_location';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case signin:
        return MaterialPageRoute(builder: (_) => SignIn());
      case sign_up_email:
        return MaterialPageRoute(
          builder: (_) => SignUpEmail(
            userRepository: UserRepository(),
          ),
        );
      case sign_in_email:
        return MaterialPageRoute(
          builder: (_) => SignInEmail(
            userRepository: UserRepository(),
          ));
      case provide_username:
        return MaterialPageRoute(builder: (_) => ProvideUsernamePage());
      case home:
        var data = settings.arguments as User;
        return MaterialPageRoute(builder: (_) => MainBottomNavigation(currentUser: data));
      case recommendation_details:
        var data = settings.arguments as Recommendation;
        return MaterialPageRoute(builder: (_) => RecommendationDetails(recommendation: data));
      case add_location:
        return MaterialPageRoute<PlacesSearchResult>(builder: (_) => AddLocationPage());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          ));
    }
  }
}
