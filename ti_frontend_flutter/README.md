# Ti
A social media app for recommending food.

## Run the Project
1. Execute `flutter doctor` to ensure the setup is completed.
2. Run the following at the project root directory.
```bash
flutter run
```

## References
1. Firebase Login with "flutter_bloc"  
<https://medium.com/flutter-community/firebase-login-with-flutter-bloc-47455e6047b0>

2. Flutter Infinite List Tutorial with "flutter_bloc"
<https://medium.com/flutter-community/flutter-infinite-list-tutorial-with-flutter-bloc-2fc7a272ec67>