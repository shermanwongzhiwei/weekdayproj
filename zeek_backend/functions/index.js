const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();
const firestore = admin.firestore();

exports.addDishFavourite = functions.firestore
  .document('favourites/{favouritesId}')
  .onCreate((snapshot, context) => {
    const favourite = snapshot.data();
    firestore
      .collection('recommendations')
      .doc(favourite.recommendationId)
      .get()
      .then(snapshot => {
        const recommendation = snapshot.data();
        firestore.collection('dishFavourites')
          .add({
            dish: recommendation.dish,
            userEmail: favourite.userEmail,
            timestamps: favourite.timestamps
          });
      });
  });

exports.removeDishFavourite = functions.firestore
  .document('favourites/{favouritesId}')
  .onDelete((snapshot, context) => {
    const favourite = snapshot.data();
    firestore
      .collection('recommendations')
      .doc(favourite.recommendationId)
      .get()
      .then((snapshot) => {
        const recommendation = snapshot.data();
        const dishFavourites = firestore
          .collection('dishFavourites')
          .where('dish', '==', recommendation.dish)
          .where('userEmail', '==', favourite.userEmail)
          .where('timestamps', '==', favourite.timestamps)
          .get();

        dishFavourites.then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            doc.ref.delete();
          });
        });
      });
  });

exports.addDishDislike = functions.firestore
  .document('dislikes/{dislikeId}')
  .onCreate((snapshot, context) => {
    const dislike = snapshot.data();
    firestore
      .collection('recommendations')
      .doc(dislike.recommendationId)
      .get()
      .then(snapshot => {
        const recommendation = snapshot.data();
        firestore.collection('dishDislikes')
          .add({
            dish: recommendation.dish,
            userEmail: dislike.userEmail,
            timestamps: dislike.timestamps
          });
      });
  });

exports.removeDishDislike = functions.firestore
  .document('dislikes/{dislikeId}')
  .onDelete((snapshot, context) => {
    const dislike = snapshot.data();
    firestore
      .collection('recommendations')
      .doc(dislike.recommendationId)
      .get()
      .then((snapshot) => {
        const recommendation = snapshot.data();
        firestore
          .collection('dishDislikes')
          .where('dish', '==', recommendation.dish)
          .where('userEmail', '==', dislike.userEmail)
          .where('timestamps', '==', dislike.timestamps)
          .get()
          .then((snapshot) => {
            snapshot.forEach((doc) => {
              doc.ref.delete();
          });
        });
      });
  });
